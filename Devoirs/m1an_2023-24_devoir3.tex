\documentclass[a4paper,12pt,reqno]{amsart}
\usepackage{lille}
\lilleset{
  titre=Devoir 3 : Espaces de Hilbert,
  solutions=true
}
\input{m1analyse}

\begin{document}

\setcounter{numeroexo}{19} % le suivant sera exo 20
% -----------------------------------------------
\begin{exo}

  Soit $H$ un espace de Hilbert complexe et $T \in \mathcal L(H)$. On appelle spectre de $T$ l'ensemble
  \[
    \sigma(T):=\ensemble{\lambda\in\mathbb{C}}{T-\lambda I\text{ n'est pas inversible}}.
  \]
  \begin{enumerate}
    \item
    \begin{enumerate}
      \item Montrer que $\forall  x,y \in H$,
      \[
        \scalprod{T(x)}{y} = \frac{1}{4}\sum_{k=0}^3i^k\scalprod{T(x+i^ky)}{x+i^ky}.
      \]
      \item Montrer que $\norm{T}\leq 4\sup\{\abs{\scalprod{Tx}{x}},\ \norm{x}=1\}$. \emph{Peut-on avoir $2$ à la place du 4 ?}
      \item En déduire que si $\forall x \in H$, $\scalprod{Tx}{x} = 0$ alors $T = 0$.
    \end{enumerate}
    \item Montrer que $\forall  x \in H, \scalprod{Tx}{x} \in \mathbb{R}$ si et seulement si $T = T^*$.
    \item \label{q3} Montrer que s'il existe $\lambda\in\mathbb{C}$ et $C>0$ tel que pour tout $x\in H$, $\norm{x}\leq C\norm{Tx-\lambda x}$, alors $T-\lambda\id$ est injectif et $\im(T-\lambda\id)$ est fermé. Si de plus $T$ est autoadjoint, montrer que $\im(T-\overline\lambda\id)$ est dense.
    \item En déduire que si $T$ est autoadjoint, alors $\sigma (T) \subseteq \mathbb{R}$.\\
    \indication{calculer $|\scalprod{(T-\lambda\id)x}{x}|^2$ pour $\lambda\in\mathbb{C}\setminus \mathbb{R}$.}
    \item En utilisant la question \eqref{q3} et en calculant $\scalprod{Tx-\lambda x}{x}$, montrer que si, $\forall  x \in H$, $\scalprod{Tx}{x} \geq 0$ alors $\sigma (T) \subseteq \mathbb{R}^{+}=[0,+\infty[$.
  \end{enumerate}
\end{exo}

\begin{solution}

  \begin{enumerate}
    \item\label{a}
    \begin{enumerate}
      \item Soient $x,y \in H$. On démontre l'égalité, de droite à gauche, en multipliant par 4 :
      \begin{multline*}
        \hl{\sum_{k=0}^3i^k\scalprod{T(x+i^ky)}{x+i^ky} =}
        \sum_{k=0}^3i^k\Big(\scalprod{T(x)}{x} + i^{k}\scalprod{T(y)}{x} + i^{-k}\scalprod{T(x)}{y} + \scalprod{T(y)}{y} \Big) = \\
        \underbrace{\sum_{k=0}^3i^k}_{=0}\scalprod{T(x)}{x} + \underbrace{\sum_{k=0}^3i^{2k}}_{=0}\scalprod{T(y)}{x} + \underbrace{\sum_{k=0}^3 1}_{=4}\scalprod{T(x)}{y} + \underbrace{\sum_{k=0}^3i^k}_{=0}\scalprod{T(y)}{y} \hl{= 4\scalprod{T(x)}{y}}.
      \end{multline*}
    \item Soit \hl{$M \coloneqq \sup\ensemble{\abs{\scalprod{T(x)}{x}}}{\norm{x}=1}$}. Ainsi $\abs{\scalprod{T(x+i^ky)}{x+i^ky}}\leq M\norm{x+i^ky}^{2}$ et on trouve, en utilisant l'identité de la question précédente, l'inégalité
      \[
        \hl{\abs{\scalprod{T(x)}{y}}} = \frac{1}{4}\abs{\sum_{k=0}^3i^k\scalprod{T(x+i^ky)}{x+i^ky}} \hl{\leq \frac{M}{4}\sum_{k=0}^3 \norm{x+i^ky}^2}.
      \]

      Pour $\norm{x}=\norm{y}=1$ nous avons \hl{$\norm{x+i^ky}\leq 2$} par l'inégalité triangulaire.
      Ainsi, pour finir, comme \hl{$\normop{T} = \sup\ensemble{\scalprod{T(x)}{y}}{\norm{x}=1, \norm{y}=1}$}
       nous trouvons
      \[
        \hl{\normop{T} \leq} \frac{M}{4}\times4\times2^2 = \hl{4\sup\ensemble{\abs{\scalprod{T(x)}{x}}}{\norm{x}=1}}.
      \]
      \emph{Nous pouvons améliorer cette inégalité et remplacer la constante 4 par 2. Pour cela au lieu d'utiliser l'inégalité triangulaire pour majorer $\norm{x+i^ky}$ on aurait pu utiliser l'identité du parallélogramme pour obtenir, dans le cas $\norm{x}=\norm{y}=1$, une majoration par $2M$ au lieu de $4M$ :
      \[
        \frac{M}{4}\sum_{k=0}^3 \norm{x+i^ky}^2 = \frac{M}{4} (4\norm{x}^2 + 4\norm{y}^2) \leq 2M.
      \]
      }
      \item D'après la question précédente, si $\forall x \in H$, $\scalprod{T(x)}{x} = 0$, alors $\\hl{normop{T}\leq} 4\times0 = \hl{0}$. Donc \hl{$T=0$}.
    \end{enumerate}
    \item\label{b} Nous avons les équivalences suivantes :
    \begin{multline*}
      \hl{\forall x \in H, \scalprod{T(x)}{x} \in \mathbb{R} \iff}
      \forall x \in H, \scalprod{T(x)}{x} = \overline{\scalprod{T(x)}{x}} \iff\\
      \forall x \in H, \scalprod{T(x)}{x} = \scalprod{x}{T(x)} \iff
      \forall x \in H, \scalprod{T(x)}{x} = \scalprod{T^*x}{x} \iff\\
      \forall x \in H, \scalprod{(T-T*)x}{x} = 0 \overset{\eqref{a}}{\iff}
      T - T^* = 0 \iff
      \hl{T = T^*} .
    \end{multline*}
    \item\label{c} Si \hl{$F \in \mathcal{L}(H)$} telle que \hl{$\norm{x}\leq C\norm{F(x)}$} pour tout $x \in H$, alors
    \begin{itemize}
      \item si $F(x) = 0$, alors $0 \geq\norm{x} \implies x=0$, donc \hl{$F$ est injective};
      \item si $F(x_n) \to l$, alors $\suite{F(x_n)}$ est de Cauchy, et donc $\suite{x_n}$ aussi, car $\norm{x_p-x_q}\leq C\norm{F(x_p)-F(x_q)}$. Ainsi, comme $H$ est complet, $x_{n}\to x \in H$ et par la continuité de $F$ on trouve $F(x_n)\to F(x) \in \im F$. Donc \hl{$\im F$ est fermé}.
    \end{itemize}
    On applique ce résultat à \hl{$F=T-\lambda\id$} pour trouver que \hl{$T-\lambda\id$ est injective à image fermée}.

    Si de plus \hl{$T=T^*$}, comme $\hl{0 =} \ker(T-\lambda\id) = \hl{\big(\im (T-\lambda\id)^*\big)^\perp}$, on trouve que $\hl{(T-\lambda\id)^* =} T^*-\overline{\lambda}\id = \hl{T-\overline{\lambda}\id}$ \hl{est à image dense}.
    \item Soit \hl{$T$ autoadjoint}. Si $\lambda = \mu + i \nu$ avec \hl{$\nu \neq 0$}, comme $T - \mu$ est autoadjoint, nous avons
    \[
      \scalprod{(T-\lambda)x}{x} = \underbrace{\scalprod{(T-\mu) x}{x}}_{\in \R} - \underbrace{i\nu\scalprod{x}{x}}_{\in i\R}.
    \]
    et donc \hl{$\abs{\scalprod{(T-\lambda)x}{x}} \geq \abs{\nu}\norm{x}^{2}$}. Comme de plus, par l'inégalité de Cauchy-Schwarz, on a \hl{$\norm{(T-\lambda)x}\norm{x} \geq \abs{\scalprod{(T-\lambda)x}{x}}$}, on trouve \hl{$\norm{(T-\lambda)x} \geq \abs{\nu}\norm{x}$} avec $\abs{\nu} > 0$. D'après la question précédente, \hl{$T-\lambda\id$ est injective à image fermée}.

    Par le même argument appliqué à $\overline{\lambda} = \mu - i \nu$ avec $\nu \neq 0$, on trouve $\norm{(T-\overline{\lambda})} \geq \abs{\nu}\norm{x}$ et donc, d'après la question précédente, \hl{$T-\lambda\id$ est} à image dense (et fermée), donc surjective, donc \hl{bijective}.

    Ainsi on vient de démontrer que si $\lambda \not\in \R$ alors $T-\lambda\id$ est bijective et donc $\lambda \not\in \sigma(T)$. Autrement dit, \hl{$\sigma (T) \subseteq \mathbb{R}$}.

    \emph{Remarque : un opérateur $F\in\mathcal{L}(H)$ est inversible dans $\mathcal{L}(H)$ si et seulement s'il est bijectif, d'après le théorème de l'isomorphisme de Banach.}

    \item Si $\forall x \in H$, \hl{$\scalprod{T(x)}{x} \geq 0$} alors, d'après \eqref{b}, $T$ est autoadjoint, et d'après la question précédente \hl{$\sigma (T) \subseteq \mathbb{R}$}. Donc \hl{il suffit de démontrer que si $\lambda \in \R_{-}^{*}$, alors $T-\lambda\id$ est inversible}.
    Soit \hl{$\lambda \in \R_{-}^{*}$}, alors $\scalprod{(T-\lambda)x}{x} = \scalprod{T(x)}{x} - \lambda\norm{x}^{2} \geq -\lambda\norm{x}^{2}$ avec $-\lambda > 0$. Ainsi, comme dans la question précédente, on trouve que \hl{$T-\lambda\id$ est} injective à image dense et fermée, donc \hl{bijective}.
  \end{enumerate}

\end{solution}

\end{document}
