\documentclass[a4paper,12pt,reqno]{amsart}
\usepackage{lille}
\lilleset{
  titre=Devoir 1 : espaces de fonctions,
  solutions=true
}
\input{m1analyse}


\begin{document}

\setcounter{numeroexo}{14} % le suivant sera exo 15
% --------------------------------------------------
\begin{exo}

  Soit $(K,d)$ un espace métrique compact, et $\mathcal{A}$ l'ensemble des fonctions lipschitziennes de $K$ dans $\mathbb{R}$.
  \begin{enumerate}
    \item Montrer que $\mathcal{A}$ est dense dans $\mathcal{C}(K,\mathbb{R})$ muni de la norme uniforme.
    \item Pour $f\in\mathcal{C}(K,\mathbb{R})$ et $\lambda>0$, on pose $f_\lambda(x)=\inf_{y\in K}\{f(y)+\lambda d(x,y)\}$.
    Montrer que $f_\lambda$ est lipschitzienne.
    \item Montrer que $\norm{f-f_\lambda}_\infty\xrightarrow[\lambda\to+\infty]{}0$ et retrouver le résultat de la première question. \emph{On pourra appliquer le théorème de Dini.}
  \end{enumerate}
\end{exo}


\begin{solution}
\newcommand*{\Lip}[1]{\ensuremath{{#1}\operatorname{\!-Lip}}}

  \begin{enumerate}
    \item Les fonctions lipschitziennes étant continues, $\mathcal{A}$ est un sous-ensemble de $\mathcal{C}(K,\mathbb{R})$. On va démontrer que $\mathcal{A}$ est une sous algèbre unifère de $\mathcal{C}(K,\mathbb{R})$ qui sépare les points. On note $\Lip{k}$ l'ensemble des fonctions $k$-lipschitziennes, de sorte que $\mathcal{A} = \bigcup_{k\geq 0}\Lip{k}$.

    Soient $f,g \in \mathcal{A}$ et $\lambda \in \mathbb{R}$, avec $f \in \Lip{k}$ et $g \in \Lip{r}$. Nous avons :
    \begin{itemize}
      \item $1 \in \mathcal{A}$ car la fonction constante $1$ est $k$-lipschitzienne pour tout $k\geq 0$ ;
      \item $\abs{\lambda f(x) - \lambda f(y)} \leq \abs{\lambda}kd(x,y)$ $\implies$ $\hl{\lambda f \in} \Lip{\lambda k}\subset \hl{\mathcal{A}}$;
      \item $\abs{(f+g)(x) - (f+g)(y)} \leq \abs{f(x) - f(y)} + \abs{g(x) - g(y)} \leq (k+r)d(x,y)$ $\implies$ $\hl{ f+g \in} \Lip{(k+r)}\subset \hl{\mathcal{A}}$;
      \item $\abs{(fg)(x) - (fg)(y)} = \abs{(f(x)g(x)-f(y)g(x)+f(y)g(x) - f(y)g(y)} \leq \abs{(f(x)-f(y))g(x)}+\abs{f(y)(g(x) - g(y))} \leq \norm{g}_{\infty}kd(x,y)+\norm{f}_{\infty}rd(x,y)$ $\implies$ $\hl{ fg \in} \Lip{(k\norm{g}_{\infty}+\norm{f}_{\infty}r)}\subset \hl{\mathcal{A}}$. Nous avons utilisé le fait que $\norm{f}_{\infty}<\infty$ et $\norm{g}_{\infty}<\infty$, car $f$ et $g$ sont des fonctions continues sur le compact $K$.
    \end{itemize}
    Ainsi \hl{$\mathcal{A}$ est une sous-algèbre unifère de $\mathcal{C}(K,\mathbb{R})$}.

    Pour tout $x \in K$, la fonction $d_x : y \mapsto d(x,y)$ est une fonction $1$-lipschitzienne par inégalité triangulaire $\implies d_{x} \in \mathcal{A}$. De plus, pour tout $y\neq x$, $d_x(x) = 0 \neq d(x,y)=d_x(y)$, donc \hl{$\mathcal{A}$ sépare les points}.

    D'après le théorème de Stone-Weierstrass, \hl{$\mathcal{A}$ est dense dans $\mathcal{C}(K,\mathbb{R})$ muni de la norme uniforme}.

    \item Nous avons $f(y) + \lambda d(x_1,y) \leq f(y) + \lambda d(x_1,x_2) + \lambda d(x_2,y)$. En prenant $\inf_{y\in K}$ des deux côtés, on trouve $f_{\lambda}(x_1) \leq f_{\lambda}(x_2) + \lambda d(x_1,x_2)$. Ainsi, par symétrie par rapport à $x_{1}$ et $x_{2}$, on trouve $\abs{f_{\lambda}(x_1) - f_{\lambda}(x_2)} \leq \lambda d(x_1,x_2)$ $\implies$ \hl{$f_{\lambda} \in \Lip{\lambda}$}.

    \item À $x$ fixé nous avons $f_{\lambda_1}(x) \leq f_{\lambda_2}(x)$ pour $\lambda_{1} \leq \lambda_{2}$. On va démontrer que $f_{\lambda}(x)\xrightarrow[\lambda\to\infty]{}f(x)$.

    Nous avons $\hl{f(x)} = f(x) + \lambda d(x,x)\hl{\geq} \inf_{y\in K}\{f(y)+\lambda d(x,y)\}=\hl{f_\lambda(x)}$ pour tout $\lambda$.

    Soit \hl{$\varepsilon > 0$}, comme $f$ est continue en $x$, $\exists\delta > 0$ tel que $d(x,y)<\delta$ $\implies$ $f(y) > f(x) - \varepsilon$. Soit $L>0$ tel que $L\geq \frac{f(x)-\varepsilon-\min f}{\delta}$, où $\min f \coloneqq \min_{y \in K} f(y) \in \mathbb{R}$ existe car $K$ est compact et $f$ est continue. Pour \hl{$\lambda \geq L$} nous avons :
    \begin{itemize}
      \item si $d(x,y) < \delta$, $\hl{f(y) + \lambda d(x,y)} \geq f(y) \hl{\geq f(x) - \varepsilon}$;
      \item si $d(x,y) \geq \delta$, $\hl{f(y) + \lambda d(x,y)} \geq f(y) + L\delta \geq f(y) + f(x) - \varepsilon - \min f \hl{\geq f(x) - \varepsilon}$.
    \end{itemize}
    Ainsi, en passant au $\inf_{y \in K}$, pour \hl{$\lambda \geq L$} nous avons \hl{$f_\lambda(x)\geq f(x)-\varepsilon$}.

    Pour conclure, comme $f$ est une fonction continue sur le compact $K$ et $f_{\lambda}$ converge simplement et de façon monotone vers $f$, d'après le théorème de Dini, \hl{la convergence $f_{\lambda}\xrightarrow[\lambda\to\infty]{}f$ est uniforme}. Ainsi tout élément de $\mathcal{C}(K,\mathbb{R})$ est limite uniforme d'éléments de $\mathcal{A}$, autrement dit \hl{$\mathcal{A}$ est dense dans $\mathcal{C}(K,\mathbb{R})$ muni de la norme uniforme}.
  \end{enumerate}

\end{solution}

\end{document}
