# [<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-500.svg" height="50" style="vertical-align: middle">](https://gitlab.com/ktzanev/m1analyse)[m1analyse](https://ktzanev.gitlab.io/m1analyse/)

Les documents du module « Analyse » du M1 Mathématiques à l'Université de Lille.

## 2024

En 2024 ce dépôt a été déplacé sur le serveur GitLab de l'Université de Lille :

https://gitlab.univ-lille.fr/tzanev/m1analyse

## 2023

Dans [ce dépôt](https://gitlab.com/ktzanev/m1analyse) vous pouvez trouver les sources LaTeX et les PDFs (compilés avec [tectonic](https://tectonic-typesetting.github.io)) des documents suivants :

- Les feuilles de td :
  - TD n°1 **[[pdf](TDs/m1an_2023-24_td1.pdf)]** [[tex](TDs/m1an_2023-24_td1.tex)]
  - TD n°2 **[[pdf](TDs/m1an_2023-24_td2.pdf)]** [[tex](TDs/m1an_2023-24_td2.tex)]
  - TD n°3 **[[pdf](TDs/m1an_2023-24_td3.pdf)]** [[tex](TDs/m1an_2023-24_td3.tex)]
  - TD n°4 **[[pdf](TDs/m1an_2023-24_td4.pdf)]** [[tex](TDs/m1an_2023-24_td4.tex)]
  - TD n°5 **[[pdf](TDs/m1an_2023-24_td5.pdf)]** [[tex](TDs/m1an_2023-24_td5.tex)]
- Les devoirs :
  - Devoir n°1 **[[pdf](Devoirs/m1an_2023-24_devoir1.pdf)]** [[tex](Devoirs/m1an_2023-24_devoir1.tex)]
  - Devoir n°2 **[[pdf](Devoirs/m1an_2023-24_devoir2.pdf)]** [[tex](Devoirs/m1an_2023-24_devoir2.tex)]
  - Devoir n°3 **[[pdf](Devoirs/m1an_2023-24_devoir3.pdf)]** [[tex](Devoirs/m1an_2023-24_devoir3.tex)]
  - Devoir n°4 **[[pdf](Devoirs/m1an_2023-24_devoir4.pdf)]** [[tex](Devoirs/m1an_2023-24_devoir4.tex)]
  - Devoir n°5 **[[pdf](Devoirs/m1an_2023-24_devoir5.pdf)]** [[tex](Devoirs/m1an_2023-24_devoir5.tex)]
- Les sujets d'examens :
  - DS1 **[[sujet](Exams/m1an_2023-24_ds1_sujet.pdf)]** **[[solutions](Exams/m1an_2023-24_ds1_solutions.pdf)]** [[tex](Exams/m1an_2023-24_ds1.tex)]
  - DS2 **[[sujet](Exams/m1an_2023-24_ds2_sujet.pdf)]** **[[solutions](Exams/m1an_2023-24_ds2_solutions.pdf)]** [[tex](Exams/m1an_2023-24_ds2.tex)]
  - DS3 **[[sujet](Exams/m1an_2023-24_ds3_sujet.pdf)]** [[tex](Exams/m1an_2023-24_ds3.tex)]

Pour compiler ces fichiers vous avez besoin de :

- [lille.sty](TDs/lille.sty) le style par défaut ;
- [m1analyse.tex](TDs/m1analyse.tex) la configuration pour ce module ;
- [lille.pdf](TDs/images/lille.pdf) le logo du département ;

Vous pouvez obtenir la totalité de [ce dépôt](https://gitlab.com/ktzanev/m1analyse) de deux façons faciles :

- en téléchargeant le [zip](https://gitlab.com/ktzanev/m1analyse/-/archive/main/m1analyse-main.zip) qui contient la dernière version des fichiers,
- en clonant le dépôt entier, l'historique y compris, en utilisant la commande `git` suivante

  ```shell
  git clone https://gitlab.com/ktzanev/m1analyse.git .
  ```

## 2022

Vous pouvez trouver les sources LaTeX et les PDFs (compilés avec [tectonic](https://tectonic-typesetting.github.io)) de 2022 à l'adresse suivante :

https://ktzanev.gitlab.io/m1analyse/2022/

Vous pouvez obtenir une copie de [la version de 2022](https://gitlab.com/ktzanev/m1analyse/-/tree/v2022) de deux façons faciles :

- en téléchargeant le [zip](https://gitlab.com/ktzanev/m1analyse/-/archive/v2022/m1analyse-v2022.zip) qui contient la dernière version des fichiers,
- en clonant le dépôt, sans l'historique complète, en utilisant la commande `git` suivante

  ```shell
  git clone --depth 1 --branch v2022 https://gitlab.com/ktzanev/m1analyse.git .
  ```

## Licence

[MIT](LICENSE)
