\documentclass[a4paper,12pt,reqno]{amsart}
\usepackage{lille}
\lilleset{
  % solutions,
  titre=\sisujet{Rattrapage}
  \sisolutions{Solutions du rattrapage},
  date=10 juin 2024,
  duree=3 heures,
}
\input{m1analyse}

% Pour que les notes de bas de page soient † et ‡
\renewcommand{\thefootnote}{\ifcase\value{footnote}\or\dag\or\ddag\or\dag\dag\or\ddag\ddag\fi}

\begin{document}
\sisujet{
  \tsvp
  \vspace{3mm}
  \attention~
  \emph{Les documents et les objets électroniques sont interdits. Les exercices sont indépendants. Toutes les réponses doivent être justifiées.}
  \vspace{4mm}
}


% -----------------------------------------------
\begin{exo} \emph{}

  \begin{enumerate}[I.]
    \item Soit $X$ un espace normé complexe et soit $A$ un opérateur linéaire et continu sur $X$.
    \begin{enumerate}[a), ref=\Roman{enumi}.\alph*]
      \item Montrer que $\ker(A^n)$ est fermé pour tout $n\ge 0$.
      \item\label{I2} Supposons que $X$ est un espace de Banach et que pour tout $x\in X$, il existe $n\ge 0$ tel que $x \in \ker(A^n)$. Montrer que $A$ est nilpotent, c'est-à-dire qu'il existe $d\in \N$ tel que $A^d = 0$.
    \end{enumerate}
    \item Soit $E = \C[X]$ l'espace des polynômes complexes et notons pour $P \in E$,
    \[
      \norm{P} \coloneqq \sum_{n=0}^{\infty} \norm{P^{(n)}}_{\infty}
    \]
    avec $ \norm{Q}_{\infty} = \sup_{x\in [0,1]}|Q(x)|$ et $P^{(n)}$ la dérivée d'ordre $n$ de $P$.
    \begin{enumerate}[a), resume]
      \item Vérifier que $\norm{}$ est bien une norme sur $E$.
      \item Montrer que l'opérateur (linéaire) de dérivation $T : E \mapsto E$ défini par $T(P) = P'$ est continu sur $E$.
      \item Montrer que pour tout $P\in E$, il existe $n\in \N$ tel que $P \in \ker(T^n)$.
      \item Montrer que $T$ n'est pas nilpotent. Expliquer ce résultat (comparer à \ref{I2}).
      \item Montrer que $\ker(T-\lambda I) = \{0\}$ pour tout nombre complexe $\lambda \neq 0$. Ici $I$ est l'opérateur identité sur $E$. Calculer $\ker(T)$.
      \item Soit $\lambda \in \C$. Montrer que pour tout $Q\in E$ il existe $P\in E$ tel que $(T - \lambda I)P = Q$.
    \end{enumerate}
  \end{enumerate}
\end{exo}

\begin{solution}

\end{solution}


\newpage
% -----------------------------------------------
\begin{exo}

  On suppose que $\suiteN{e_n}$ est une base orthonormale dans un espace de Hilbert $H$ et que $\suiteN{v_n}$ est un système orthonormal dans $H$, c'est-à-dire $\norm{v_n} = 1$ pour tout $n$ et $\scalprod{v_i}{v_j} = 0$ pour $i\neq j$.  Le but de cet exercice est de montrer que si
  $\sum_{n=0}^{\infty} \norm{v_n -e_n}^2 < \infty$ alors $\suiteN{v_n}$ est aussi une base orthonormale de $H$.

  \begin{enumerate}
    \item\label{2a} Montrer que si $M$ et $N$ sont deux sous-espaces de $H$ de dimension finie avec $\text{dim} (M) < \text{dim} (N)$, alors $M^{\perp} \cap N \neq \{0_H\}$.
    \item\label{2b} Supposons que $\sum_{n=0}^{\infty} \norm{v_n -e_n}^2 < \infty$ et que $\suiteN{v_n}$ n'est pas une base orthonormale de $H$. Justifier l'existence d'un vecteur $u \in H$ tel que $\norm{u} = 1$ et $u \perp v_n$ pour tout $n$ et d'un entier $N$ tel que $\sum_{n=N+1}^{\infty}  \norm{v_n -e_n}^2 < 1$.
    \item En utilisant \eqref{2a}, montrer l'existence de $w \neq 0$ dans l'espace engendré par $\{u, v_0, v_1, \cdots, v_N\}$ tel que $w \perp e_j$ pour $0\le j \le N$.
    \item Trouver une contradiction en utilisant $\sum_{n=N+1}^{\infty}  \norm{v_n -e_n}^2 < 1$ et conclure.
  \end{enumerate}
\end{exo}

\begin{solution}

\end{solution}

% -----------------------------------------------
\begin{exo}

  \begin{enumerate}[I.]
    \item Soit $H$ un espace de Hilbert complexe et soit $T:H \mapsto H$ un opérateur linéaire avec $\norm{T} \le 1$.
    \begin{enumerate}[a)]
      \item Montrer que si $x\in H$, alors $Tx = x$ si et seulement si $\scalprod{Tx}{x} = \norm{x}^2$.\\
      \indication{pour l'implication difficile on pourra utiliser le cas d'égalité dans l'inégalité de Cauchy-Schwarz, ou estimer $\norm{Tx-x}^2$.}
      \item En déduire que
      \[
        \ker(I-T) = \ker(I-T^*),
      \]
      où $T^*$ est l'adjoint hilbertien de $T$.
      \item Montrer que
      \[
        \im(I-T)^{\perp} = \ker(I-T).
      \]
      \item En déduire que
      \[
        H = \ker(I-T) \oplus \overline{\im(I-T))}.
      \]
    \end{enumerate}
    \item On note, pour $n\ge 0$,
    \[
      M_n(T) = \frac{I+T+ \cdots + T^{n}}{n+1}.
    \]
    Montrer que pour tout $x\in H$,
    \[
      \lim_{n\to\infty} \norm{ M_n(T)x - Px} = 0,
    \]
    où $P$ est la projection orthogonale sur  $\ker(I-T)$.\\
    \indication{on considérera successivement les cas $x \in \ker(I-T)$, $x \in  \im(I-T)$ et $x \in  \overline{\im(I-T)}$.}
  \end{enumerate}
\end{exo}

\begin{solution}

\end{solution}

\end{document}
