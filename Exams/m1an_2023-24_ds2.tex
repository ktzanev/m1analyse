\documentclass[a4paper,12pt,reqno]{amsart}
\usepackage{lille}
\lilleset{
  % solutions,
  titre=\sisujet{Examen}
  \sisolutions{Solutions de l'examen},
  date=18 décembre 2023,
  duree=3 heures,
}
\input{m1analyse}

% Pour que les notes de bas de page soient † et ‡
\renewcommand{\thefootnote}{\ifcase\value{footnote}\or\dag\or\ddag\or\dag\dag\or\ddag\ddag\fi}

\begin{document}
\sisujet{
  \tsvp
  \vspace{3mm}
  \attention~
  \emph{Les documents et les objets électroniques sont interdits. Les exercices sont indépendants. Toutes les réponses doivent être justifiées.}
  \vspace{4mm}
}


% -----------------------------------------------
\begin{exo} \emph{(Convolution et espaces $L^{p}$)}

  Soient $f$ et $g$ deux fonctions mesurables sur $(\R,\mathcal{B}(\R))$.
  \begin{enumerate}
    % ~~~~~~~~~~~~~~~~~~~~~~
    \item Soient $p \in [1,+\infty]$ et $q$ l'exposant conjugué de $p$. Montrer que si $f \in L^p(\R)$ et $g \in L^{q}(\R)$ alors $f \ast g$ est bien définie sur $\R$ et vérifie $\norm{f \ast g}_\infty \leq\norm{f}_p\norm{g}_{q}$.

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item On suppose que $f$ et $g$ sont dans $L^1(\R)$. Montrer que $f \ast g$ est définie presque partout, appartient à $L^1(\R)$ et vérifie $\norm{f \ast g}_1\leq \norm{f}_1\norm{g}_1$.
  \end{enumerate}
  Pour la suite de l'exercice, considérons $f \in L^1(\R)$ et $g \in L^p(\R)$ avec $1<p<\infty$.
  \begin{enumerate}[resume]
    % ~~~~~~~~~~~~~~~~~~~~~~
    \item En écrivant $\abs{f(x-y)}\abs{g(y)}=\abs{f(x-y)}^{\frac{1}{p}}\abs{g(y)}\abs{f(x-y)}^{\frac{1}{q}}$, montrer que $f \ast g$ existe presque partout, est dans $L^p(\R)$ et satisfait $\norm{f \ast g}_p\leq \norm{f}_1\norm{g}_p$.

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item Soit $T_g:L^1(\R)\to L^p(\R)$ l'opérateur de convolution défini pour $h \in L^1(\R)$ par $T_g(h)=h \ast g$. Montrer que $T_g$ est bien défini et continu. En testant $T_g$ contre une unité approchée, montrer que $\norm{T_g}=\norm{g}_p$.

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item Soit $S_f:L^p(\R)\to L^p(\R)$ défini pour $h \in L^p(\R)$ par $S_f(h)=f \ast h$. Montrer que $S_f$ est bien défini et continu. Et, si $f$ est positive, montrer que $\norm{S_f}=\norm{f}_1$.\\
    \indication{Pour $f$ positive, montrer que la suite $\suite{f_n}$ définie par $f_n(x)=\frac{1}{\norm{f}_1}nf(nx)$ est une unité approchée. Puis considérer $S_{f}(h_n)$ pour $h_{n}(x)=h(\frac{x}{n})$.}
  \end{enumerate}
\end{exo}

\begin{solution}

  \emph{Il s'agit de l'exercice 7 de la feuille de TD n°5. Les questions (a) et (b) étaient faites en cours et les autres questions étaient faites en TD.}
  \begin{enumerate}
    % ~~~~~~~~~~~~~~~~~~~~~~
    \item On pose $g_x(t) \coloneqq g(x-t)$ pour $x \in \R$ et $t \in \R$. Alors $g_x \in L^q(\R)$ et $\norm{g_x}_q = \norm{g}_q$, par le simple changement de variable $s \coloneqq x-t$, pour tout $x \in \R$ et $q \in [1, \infty]$.
    D'après l'inégalité de Hölder, comme $f \in L^p(\R)$ et $g_x \in L^q(\R)$, on a $f(t)g_x(t) \in L^1(\R)$, pour tout $x$ et $\norm{fg_x}_1 = \int_\R \abs{f(t)g_x(t)}\dd t \leq \norm{f}_p\norm{g_x}_q = \norm{f}_p\norm{g}_q$.

    Ainsi $f \ast g(x) = \int_\R f(t)g_x(t)\dd t$ existe pour tout $x \in \R$ et vérifie $\norm{f \ast g}_\infty \leq\norm{f}_p\norm{g}_{q}$, car pour tout $x \in \R$, on a $\abs{f \ast g(x)} \leq \int_\R \abs{f(t)g_x(t)}\dd t \leq \norm{f}_p\norm{g}_q$.

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item Comme $\abs{f(t)g(x-t)}$ est une fonction mesurable à valeurs positives nous avons, en appliquant le théorème de Fubini,
    \begin{multline*}\qquad
      \norm{f \ast g}_1 =
      \int_\R \int_\R \abs{f(t)g(x-t)} \dd t\dd x =
      \int_\R \abs{f(t)}\int_\R \abs{g(x-t)}\dd x \dd t =\\
      \int_\R \abs{f(t)}\norm{g}_1 \dd t =
      \norm{f}_1 \norm{g}_1 < \infty.
    \end{multline*}
    Donc, par Fubini, $f \ast g$ est défini presque partout, là où $\abs{f} \ast \abs{g} < \infty$, appartient à $L^1(\R)$ et vérifie $\norm{f \ast g}_1\leq \norm{f}_1\norm{g}_1$.
  \end{enumerate}
  Pour la poursuite de l'exercice, considérons $f \in L^1(\R)$ et $g \in L^p(\R)$ avec $1<p<\infty$.
  \begin{enumerate}[resume]
    % ~~~~~~~~~~~~~~~~~~~~~~
    \item En appliquant l'inégalité triangulaire, puis l'inégalité de Hölder, on trouve
    \begin{multline*}\qquad
      \abs{\int_\R f(t)g(x-t)\dd t} \leq\\
      \int_\R \abs{f(t)g(x-t)}\dd t =
      \int_\R \abs{f(t)}^{1/p}\abs{g(x-t)}\abs{f(t)}^{1/q} \leq\\
      \p{\int_\R \abs{f(t)}\abs{g(x-t)}^p\!\dd t}^{1/p}\norm{f}_1^{1/q}.
    \end{multline*}

    En utilisant cette dernière inégalité, puis le théorème de Fubini, nous avons
    \begin{multline*}\qquad
      \norm{f \ast g}_p^p = \int_\R \abs{\int_\R f(t)g(x-t)\dd t}^p\!\dd x \leq\\
      \int_\R \p{\int_\R \abs{f(t)}\abs{g(x-t)}^p\!\dd t}^{p/p}\norm{f}_1^{p/q}\dd x =
      \norm{f}_1^{p/q}\int_\R \int_\R \abs{f(t)}\abs{g(x-t)}^p\!\dd t\dd x =\\
      \norm{f}_1^{p/q}\int_\R \abs{f(t)}\int_\R \abs{g(x-t)}^p\!\dd x\dd t =
      \norm{f}_1^{p/q}\int_\R \abs{f(t)}\norm{g}_p^p\!\dd t =\\
      \norm{f}_1^{p/q}\norm{g}_p^p\norm{f}_1 = \norm{f}_1^p\norm{g}_p^p.
    \end{multline*}

    Ainsi $f \ast g$ existe presque partout, est dans $L^p(\R)$ et satisfait $\norm{f \ast g}_p\leq \norm{f}_1\norm{g}_p$.

    \emph{Remarque : Cette inégalité est trivialement vérifiée aussi pour $p=1$ et $p=\infty$, donc la question aurait pu être posée pour $1\leq p\leq \infty$.}

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item L'opérateur $T_g$ est bien défini d'après la question précédente. Il est linéaire, car le produit de convolution est bi-linéaire. De plus, pour tout $f \in L^p(\R)$, d'après la question précédente, $\norm{T_g(f)}_p \leq \norm{f}_1\norm{g}_p$. Ainsi $T_g$ est continu et $\norm{T_g} \leq \norm{g}_p$.

    Soit $\suite{\phi_n}$ une unité approchée. D'après le cours $\norm{g - \phi_n \ast g }_p \xrightarrow[n\to\infty]{} 0$. Et comme la norme est continue, on obtient $\norm{T_g(\phi_n)}_p \xrightarrow[n\to\infty]{} \norm{g}_p$. De plus, comme $\norm{\phi_n}_1 = 1$ pour tout $n$, on obtient $\norm{T_g} \geq \norm{g}_p$. Ainsi $\norm{T_g} = \norm{g}_p$.

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item Comme dans la question précédente, $S_f$ est bien défini, d'après (i), pour tout $g \in L^p(\R)$, linéaire, d'après la bilinéarité de la convolution, et en utilisant l'inégalité de (i), on trouve $\norm{S_f(g)}_p \leq \norm{f}_1\norm{g}_p$ pour tout $g \in L^p(\R)$. Ainsi $S_f$ est continu et $\norm{S_f} \leq \norm{f}_1$.

    Soit $f$ une fonction positive non nulle. On pose $f_n(x) \coloneqq \frac{1}{\norm{f}_1}nf(nx)$ pour tout $n \geq 1$. Alors, par le changement de variable $t \coloneqq nx$, on trouve $\int_\R f_n(t)\dd t = 1$. Et comme $f_n$ est positive, $\norm{f_n}_1 = 1$ pour tout $n \geq 1$. De plus, par le même changement de variable $t \coloneqq nx$, $\int_{\abs{x} > \varepsilon} n\abs{f(nx)}\dd x = \int_{\abs{t} > n\varepsilon} \abs{f(t)}\dd t \xrightarrow[n\to\infty]{} 0$ pour tout $\varepsilon > 0$, d'après le théorème de la convergence dominée, car $f \in L^1(\R)$. Ainsi la suite $\suite[n\geq 1]{f_n}$ est une unité approchée, car elle vérifie
    \begin{itemize}
      % ~~~~~~~~~~~~~~~~~~~~~~
      \item $\int_\R f_n(t)\dd t = 1$ pour tout $n \geq 1$,
      % ~~~~~~~~~~~~~~~~~~~~~~
      \item $\norm{f_n}_1 = 1$ pour tout $n \geq 1$, donc est uniformément bornée,
      % ~~~~~~~~~~~~~~~~~~~~~~
      \item $\int_{\abs{x} > \varepsilon} \abs{f_n(x)}\dd x \xrightarrow[n\to\infty]{} 0$ pour tout $\varepsilon > 0$.
    \end{itemize}

    Soit $h \in L^p(\R)$. On pose $h_n(x) \coloneqq \abs{h(\frac{x}{n})}$ pour tout $n\geq 1$. Nous avons $\norm{h_n}_p = \p{n\int_\R \abs{h(\frac{x}{n})}^{p}\dd\frac{x}{n}}^{1/p} =  n^{1/p}\norm{h}_p$ pour tout $n \geq 1$.

    Nous avons aussi
    \begin{multline*}\hspace{1cm}
      \norm{S_f(h_n)}_p^p =
      \int_\R \abs{f \ast h_n(x)}^p\!\dd x =\\
      \int_\R \abs{\int_\R f(t)h_n(x-t)\dd t}^p\!\dd x =\\
      \int_\R \abs{\norm{f}_1\int_\R \frac{1}{\norm{f}_1} nf(n\frac{t}{n})h(\frac{x}{n}-\frac{t}{n})\dd\frac{t}{n}}^p\!n\dd\frac{x}{n}.
    \end{multline*}
    Ainsi par les changements de variables $\frac{t}{n}\to t$ et $\frac{x}{n}\to x$, on trouve
    \[
      \norm{S_f(h_n)}_p^p = n\norm{f}_1^p \int_\R \abs{\int_\R f_n(t)h(x-t)\dd t}^p\!\dd x = n\norm{f}_1^p\norm{f_n \ast h}_p^p.
    \]
    Comme $\norm{f_n \ast h}_p\to\norm{h}_p$, nous obtenons $\frac{\norm{S_f(h_n)}_p}{\norm{h_n}_p} = \frac{n^{1/p}\norm{f}_1\norm{f_n \ast h}_p}{n^{1/p}\norm{h}_p} \xrightarrow[n\to\infty]{} \norm{f}_1$. Donc $\normop{S_f} \geq \norm{f}_1$. Ainsi $\normop{S_f} = \norm{f}_1$.
  \end{enumerate}
\end{solution}


% -----------------------------------------------
\begin{exo} \emph{(Densité et coefficients de Fourier)}

  On considère l'espace de Hilbert $H = L^2([0,1],\lambda)$, où $\lambda = \dd x$ désigne la mesure de Lebesgue, muni de la norme $\norm{}_2$. Démontrer que l'ensemble
  \[
    A \coloneqq \ensemble[\Big]{f \in C^\infty([0,1])}{\int_0^1 \frac{1}{x}f(x) \dd x = 0}
  \]
  est dense dans $H$.\\
  \indication{
    Montrer que $A^{\perp} = 0$  en considérant les fonctions $f_{n}(x)=xe^{2i\pi nx}, n \in \Z^*$.
  }
\end{exo}

\begin{solution}

  Soient $f_{n}(x)=xe^{2i\pi nx}, n \in \Z^*$. Ces fonctions sont clairement $C^\infty$ et on a $\int_0^1 \frac{1}{x}f_{n}(x) \dd x = \int_0^1 e^{2i\pi nx} \dd x = 0$ pour $n \in \Z^*$. Ainsi $f_n \in A$ pour tout $n \in \Z^*$. Considérons $g \in A^\perp$. Alors pour tout $n \in \Z^*$, on a $\scalprod{g}{f_n} = \int_0^1 g(x)xe^{2i\pi nx} \dd x = 0$, donc la fonction $xg(x)$ est une constante, car elle est orthogonale à tous les éléments de la base hilbertienne $e_n = e^{2i\pi nx}$, sauf possiblement à $e_0 = 1$. Ainsi, $g(x) = c/x$ pour une certaine constante $c \in \C$. Comme $g \in L^2([0,1])$ et $\frac{1}{x}\notin L^2([0,1])$, on a $c = 0$ et donc $g = 0$. Ainsi $A^\perp = 0$ et donc $A$ est dense dans $H$.
\end{solution}


% -----------------------------------------------
\def\HS{\mathcal{HS}}
\begin{exo} \emph{(Opérateurs de Hilbert-Schmidt)}

  Soit $E$ un espace de Hilbert complexe séparable et de dimension infinie. On note $\norm{}$ sa norme et $\scalprod{}{}$ son produit scalaire.

  \begin{enumerate}
    % ~~~~~~~~~~~~~~~~~~~~~~
    \item Soient $\suite[n\ge0]{e_n}$ et $\suite[p\ge0]{f_p}$ deux bases hilbertiennes de $E$. Démontrer que si $T$ est un opérateur linéaire et continu sur $E$, alors
    \[
      \sum_{n=0}^{+\infty} \norm{Te_n}^2 = \sum_{n=0}^{+\infty} \norm{T^*f_p}^2 \le +\infty ,
    \]
    où $T^*$ est l'adjoint hilbertien de $T$.\\
    \indication{Si $u_{n,p}$ sont positifs, alors $\sum_n\sum_p u_{n,p} = \sum_p\sum_n u_{n,p}$.}

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item En déduire que
    \[
      \sum_{n=0}^{+\infty} \norm{Te_n}^2 = \sum_{n=0}^{+\infty} \norm{Tf_p}^2 .
    \]
  \end{enumerate}

  On fixe désormais une base hilbertienne $\suite[n\ge0]{e_n}$ de $E$. On note $\HS(E)$\footnote{$\HS(E)$ est l'espace des opérateurs dit \emph{de Hilbert-Schmidt} sur $E$.} l'espace vectoriel de tous les opérateurs $T$ linéaires et continus sur $E$ tels que
  \[
    \norm{T}_{\HS} := \sqrt{\sum_{n=0}^{+\infty} \norm{Te_n}^2} < +\infty.
  \]

  \begin{enumerate}[resume]
    % ~~~~~~~~~~~~~~~~~~~~~~
    \item Démontrer que $\HS(E)$ ne coïncide pas avec $\mathcal{B}(E)$, l'espace des opérateurs linéaire bornés sur $E$\footnote{noté également $\mathcal{L}(E)$.}, et que pour tout $T \in \HS(E)$ on a $\normop{T} \le \norm{T}_{\HS}$.

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item Démontrer que $\norm{}_{\HS}$ est une norme sur $\HS(E)$ qui provient d'un produit scalaire, qu'on explicitera.\\
    \remarque{On ne demande pas de montrer la complétude de l'espace $(\HS(E), \norm{}_{\HS})$.}

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item Soit $T \in \HS(E)$. On note, pour $n\ge 0$, $P_n$ la projection orthogonale sur l'espace vectoriel engendré par $e_j$, $0\le j\le n$. Démontrer que $\lim_{n\to\infty} \norm{T-TP_n}_{\HS} = 0$.
  \end{enumerate}
\end{exo}

\begin{solution}
  \begin{enumerate}
    % ~~~~~~~~~~~~~~~~~~~~~~
    \item On pose $u_{n,p} = \abs{\scalprod{Te_n}{f_p}}^2 = \abs{\scalprod{e_n}{T^*f_p}}^{2}$. Comme $\suite[p\ge0]{f_p}$ est une base hilbertienne de $E$, nous avons
    $\norm{Te_n}^2 = \sum_{p=0}^{+\infty} \abs{\scalprod{Te_n}{f_p}}^2 = \sum_{p=0}^{+\infty} u_{n,p}$ et donc $\sum_{n=0}^{+\infty} \norm{Te_n}^2 = \sum_{n=0}^{+\infty}\sum_{p=0}^{+\infty} u_{n,p}$. De même, en utilisant que $\suite[n\ge0]{e_n}$ est une base hilbertienne, on trouve $\sum_{p=0}^{+\infty} \norm{T^*f_p}^2 = \sum_{p=0}^{+\infty}\sum_{n=0}^{+\infty} u_{n,p}$. Comme les $u_{n,p}$ sont positifs, on peut permuter les sommes et on obtient
    \[
      \sum_{n=0}^{+\infty} \norm{Te_n}^2 = \sum_{n=0}^{+\infty} \norm{T^*f_p}^2 \le +\infty.
    \]

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item\label{q:1b} En appliquant le résultat précédent à $\suite[n\geq 0]{e_n} = \suite[p\geq 0]{f_p}$, on obtient $\sum_{p=0}^{+\infty} \norm{Tf_p}^2 = \sum_{p=0}^{+\infty} \norm{T^*f_p}^2$. Ainsi, en combinant avec le résultat précédent, on trouve
    \[
      \sum_{n=0}^{+\infty} \norm{Te_n}^2 = \sum_{n=0}^{+\infty} \norm{Tf_p}^2 .
    \]

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item L'identité $I$ sur $E$ est dans $\mathcal{B}(E)$, mais pas dans $\HS(E)$. En effet, $\norm{I}_{\HS} = \sqrt{\sum_{n=0}^{+\infty} \norm{Ie_n}^2} = \sqrt{\sum_{n=0}^{+\infty} 1} = +\infty$. Et plus généralement, toute isométrie est dans $\mathcal{B}(E)$, mais pas dans $\HS(E)$ par le même raisonnement. Ainsi, $\HS(E) \neq \mathcal{B}(E)$.

    Par l'inégalité de Cauchy-Schwarz, nous avons $\abs{\scalprod{Tf}{e_n}} = \abs{\scalprod{f}{T^*e_n}} \leq \norm{f}\norm{T^*e_n}$. Ainsi
    \[
      \norm{Tf}^2 = \sum_{n=0}^{+\infty} \abs{\scalprod{Tf}{e_n}}^2 \leq \norm{f}^2 \sum_{n=0}^{+\infty} \norm{T^*e_n}^2 = \norm{f}^2 \norm{T^*}_{\HS}^2,
    \]
    donc  $\normop{T} \leq \norm{T^*}_{\HS} = \norm{T}_{\HS}$. L'égalité $\norm{T^*}_{\HS} = \norm{T}_{\HS}$ est une conséquence de la question \eqref{q:1b}.

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item Pour deux opérateurs $T_1, T_2 \in \HS(E)$ on pose $\scalprod{T_1}{T_2}_\HS \coloneqq \sum_{n=0}^\infty \scalprod{T_1e_n}{T_2e_n}$. Le fait que cette application soit bilinéaire symétrique est une conséquence de la bilinéarité et de la symétrie du produit scalaire sur $E$. De plus, pour $T \in \HS(E)$ si $\scalprod{T}{T}_\HS = 0$, alors $\forall n \geq 0, \scalprod{Te_n}{Te_n} = 0 \implies \forall n \geq 0, Te_n = 0 \implies T = 0$. Ainsi, $\scalprod{\cdot}{\cdot}_\HS$ est un produit scalaire sur $\HS(E)$. De plus, pour tout $T \in \HS(E)$, on a $\scalprod{T}{T}_\HS = \sum_{n=0}^\infty \norm{Te_n}^2 = \norm{T}_{\HS}^2$ et donc $\norm{T}_{\HS} = \sqrt{\scalprod{T}{T}_\HS}$ est une norme.

    % ~~~~~~~~~~~~~~~~~~~~~~
    \item Nous avons $(T-TP_n)(e_k) = 0$ si $k \leq n$ et $(T-TP_n)(e_k) = Te_k$ si $k > n$. Ainsi
    \[
      \norm{T-TP_n}_{\HS} = \sum_{k=n+1}^\infty \norm{Te_k}^2 \xrightarrow[n\to\infty]{}0,
    \]
    car il s'agit du reste d'une série convergente.
  \end{enumerate}
\end{solution}


\end{document}

