\documentclass[a4paper,12pt,reqno]{amsart}
\usepackage{lille}
\lilleset{
  solutions,
  titre=\sisujet{Devoir surveillé}\sisolutions{Solutions du devoir surveillé},
  date=8 novembre 2023,
  duree=2 heures,
}
\input{m1analyse}

\begin{document}
\sisujet{
  \tsvp
  \vspace{4mm}
  \attention~
  \emph{Les documents et les objets électroniques sont interdits. Les exercices sont indépendants. Toutes les réponses doivent être justifiées.}
  \vspace{7mm}
}

% -----------------------------------------------
\begin{exo}

  Soient $a,b \in \R$ tels que $0 < a < b < 1$. Le but du problème est de montrer que chaque fonction réelle continue sur $I = [a,b]$ est limite uniforme sur $I$ d'une suite de fonctions polynômes à coefficients dans $\Z$.

  Soit $\varphi : [0,1] \to \R$, $\varphi(x) = 2x(1-x)$.

  \begin{enumerate}
    \item Étudier la monotonie et la convergence de la suite définie par récurrence:
    \[
      a_{0} \in [0,1]\quad\text{et}\quad \forall n\ge 1,\quad a_{n} = \varphi(a_{n-1}).
    \]
  \end{enumerate}

    On considère la suite des fonctions polynomiales $\suiteN{P_n}$ définie sur $[0,1]$ par
    \[
      P_0(x) = x\quad\text{et}\quad \forall n\ge 1,\quad P_{n}(x) = \varphi \big(P_{n-1}(x)\big).
    \]

  \begin{enumerate}[resume]
    \item En déduire la convergence simple de $\suiteN{P_n}$ sur $[0,1]$ vers une fonction à préciser.

    \item Montrer que $\suiteN{P_n}$ converge uniformément sur $I=[a,b]$ vers une fonction à préciser.
  \end{enumerate}

  On note $\mathcal{A} \coloneqq \overline{\Z[X]}$, l'adhérence des fonctions polynomiales à coefficients entiers dans $C(I,\R)$ muni de la norme sup sur $I=[a,b]$.

  \begin{enumerate}[resume]
    \item Montrer que $\mathcal{A}$ est stable pour la somme et le produit de $C(I,\R)$.

    \item Montrer que $\mathcal{A}$ contient toutes les fonctions constantes réelles.\\
    \indication{on pourra utiliser sans preuve que les nombres rationnels dyadiques $\{\frac{p}{2^m} : (p,m)\in \Z\times \N\}$ sont denses dans $\R$.}

    \item Montrer que $\mathcal{A}$ coïncide avec $C(I,\R)$.
  \end{enumerate}
\end{exo}

\begin{solution}

  \begin{enumerate}
    \item\label{suite1}\imageR[width=49mm, sep=7mm, y offset=17]{img_ds1_suite}{
    La fonction $\varphi$ est un polynôme de degré 2, avec $0$ et $1$ comme racines, qui admet un maximum global $\varphi(\frac{1}{2}) = \frac{1}{2}$. Nous avons $[0,\frac{1}{2}] = \varphi([0,1])$ qui est un intervalle stable ($\varphi([0,\frac{1}{2}]) = [0,\frac{1}{2}]$) et comme $\varphi$ est croissante avec $\varphi(x)\geq x$ sur $[0,\frac{1}{2}]$, on déduit le comportement de la suite récurrente $\suiteN{a_n}$:
    }
    \begin{itemize}
       \item \hl{si $a_{0}=0$} ou \hl{$a_{0}=1$}, la suite $\suite[n\geq1]{a_n}$ est nulle, donc \hl{admet $0$ pour limite};
       \item \hl{si $a_{0} \in ]0,1[$}, alors $a_{1} \in ]0,\frac{1}{2}]$ et la suite \hl{$\suite[n\geq1]{a_n}$ est croissante} majorée et \hl{converge vers} son sup qui est le point fixe \hl{$\frac{1}{2}$} de $\varphi$.
     \end{itemize}

    \item\label{suite2} D'après la question précédente, la suite $\suiteN{P_n(x)}$ admet $0$ pour limite si $x=P_{0}(x) \in \{0,1\}$ et $\frac{1}{2}$ si $x \in ]0,1[$. Autrement dit, la suite \hl{$\suiteN{P_n}$ converge simplement vers} la fonction:
    \[\hl{
      x\mapsto
      \begin{cases}
        0           & \text{si }x \in \{0,1\} \\
        \frac{1}{2} & \text{si }x \in ]0,1[
      \end{cases}
      }
    \]

    \item\label{suite3} D'après les questions \eqref{suite1} et \eqref{suite2}, sur le compact $[a,b]\subset]0,1[$, la suite $\suite[n\geq 1]{P_n(x)}$ est croissante et converge simplement vers la fonction constante (continue) $\frac{1}{2}$. Donc, d'après le théorème de Dini, \hl{cette convergence est uniforme}.\\
    \emph{Remarque: en utilisant que $\forall x \in [a,b],\forall n\geq 1, P_{n}(x) \in [c_{n},\frac{1}{2}]$ pour $c_{n+1}=\varphi(c_{n})$ et $c_{1} = \min\{\varphi(a),\varphi(b)\}$, avec $c_{n}\to \frac{1}{2}$, on déduit la convergence uniforme sans l'utilisation du théorème de Dini.}

    \item Si $f_{n}\to f$ et $g_{n}\to g$ dans $C(I,\R)$ avec $\norminf{}$, en utilisant qu'il existe $M>0$ tel $\norminf{g_{n}}<M$, alors
    \begin{itemize}
      \item $\hl{\norminf{(f+g)-(f_n+g_n)}}\leq\norminf{(f-f_n)}+\norminf{(g-g_n)}\hl{\to0}$;
      \item $\hl{\norminf{(fg-f_ng_n)}} = \norminf{(fg-fg_n+fg_n-f_ng_n)} \leq \norminf{f}\norminf{(g-g_n)}+\norminf{f-f_n}M$ $\hl{\to0}$.
    \end{itemize}
    Et comme \hl{$\Z[X]$ est stable par addition et multiplication}, on déduit que sa fermeture \hl{$\mathcal{A}$ l'est également}.
    \item L'ensemble $\Z[X]$ contient les fonctions constantes entières $x\mapsto p \in \N$. D'après la question \eqref{suite3}, $\mathcal{A}$ contient aussi la fonction constante $\frac{1}{2}$. Comme $\mathcal{A}$ est stable par produit, d'après la question précédente, il contient les fonctions constantes à valeurs dans les nombres rationnels dyadiques $\ensemble{\frac{p}{2^m}}{(p,m)\in\Z\times \N}$. Et par densité de ces derniers dans $\R$ et comme $\mathcal{A}$ est fermé, on peut conclure que \hl{$\mathcal{A}$ contient toutes les fonctions constantes sur $[a,b]$}.
    \item En utilisant encore une fois la stabilité de $\mathcal{A}$ par produit et le fait que $\mathcal{A}$ contient les fonctions constantes, on peut conclure qu'il est stable par multiplication par $\R$, donc \hl{$\mathcal{A}$ est une algèbre unitaire}. De plus \hl{$\mathcal{A}$ sépare les points} car $x\mapsto x \in \mathcal{A}$. \hl{D'après le théorème de Stone-Weierstrass}, $\mathcal{A}$ est dense dans $C(I,\R)$, et comme $\mathcal{A}$ est fermée on peut conclure que \hl{$\mathcal{A}=C(I,\R)$}.
  \end{enumerate}
\end{solution}

\newpage
% -----------------------------------------------
\begin{exo}

  Soit $E= \ell^{\infty}(\N,\R)$ l'espace de Banach de toutes les suites réelles bornées $x = \suiteN{x_n}$ muni de la norme
  \[
    \norminf{x} = \sup_{n\ge 0}\abs{x_n} .
  \]
  On note $\ind$ la suite constante égale à $1$. Soit l'application $S : E \to E$ définie par
  \[
    S(x_0, x_1, x_2, \dots) = (x_1, x_2, \dots).
  \]
  On appelle \emph{limite de Banach} une forme linéaire $L\in E^{*}$ telle que
  \begin{enumerate}[(i)]
    \item\label{cond i} $\normop{L} = L(\ind) = 1$ ;
    \item\label{cond ii} $L(Sx) = L(x)$ pour tout $x\in E$.
  \end{enumerate}

  \begin{enumerate}
    \item Montrer que si la suite $x=(x_n)$ converge vers $c\in \R$, alors $L(x) = c$ pour $L$ limite de Banach.
    \indication{considérer la suite $\suiteN{y_n}$ avec $y_{n} = x_{n} - c$ et démontrer que $y_n\to 0 \iff \norminf{S^{n}(y)}\to 0$.}

    \item On note $W = \ensemble{x-Sx}{x\in E}$. Soit $\dist(\ind, W)$, la distance de $\ind$ à $W$. Expliquer pourquoi $W$ est un sous-espace vectoriel de $E$.
    Montrer que $\dist(\ind, W)=1$ si et seulement si $E$ possède une limite de Banach.
    \indication{pour démontrer que si $\dist(\ind, W) = 1$ alors il existe une limite de Banach sur E, utiliser une des variantes du théorème de \text{Hahn-Banach}.}

    \item Soient $a$ et $b$ deux nombres réels. Montrer que si $L$ est une limite de Banach, alors
    \[
      L((a,b,a,b, a, \dots)) = \frac{a+b}{2}.
    \]

    \item Montrer qu'il n'y a pas de limite de Banach multiplicative, \emph{i.e.\,} une limite de Banach telle que $L(ab) = L(a)L(b)$, où $a = (a_n)_{n\ge0}$, $b = (b_n)_{n\ge0}$ et $ab = (a_nb_n)_{n\ge0}$.

  \end{enumerate}

  Soit $M$ le sous-espace vectoriel de toutes les suites $x\in E$ telles que la limite de $\frac{x_0 + x_1 + \dots + x_{n-1}}{n}$ existe dans $\R$ et soit $\ell : M \to \R$ la fonctionnelle linéaire définie par
  \[
    \ell(x) = \lim_{n\to +\infty} \frac{x_0 + x_1 + \dots + x_{n-1}}{n}.
  \]

  \begin{enumerate}[resume]
    \item Calculer $\normop{\ell}$.

    \item Montrer que si $x\in E$, alors $x-Sx \in M$. Calculer $\ell(x-Sx)$.

    \item Montrer l'existence d'une limite de Banach.
  \end{enumerate}
\end{exo}

\begin{solution}

  \begin{enumerate}
    \item Soit $x=\suiteN{x_n}$ avec $\lim x_n = c$, on pose $y=x-c$ avec $\lim y_n = 0$. \hl{Pour démontrer que $L(x)=c$ il suffit de démontrer que $L(y)=0$}, car $L(x)-c = L(x)-cL(\ind) = L(x-c\ind) = L(y)$ d'après \ref{cond i}. Soit $\varepsilon>0$, alors il existe $N$ tel que $\abs{y_{n}}<\varepsilon$ pour $n\geq N$. Cette dernière condition peut se traduire en $\norminf{S^{n}(y)} < \varepsilon$ pour $n > N$, autrement dit $S^{n}(y)\to 0$ dans $E$. Et comme $L$ est une forme linéaire continue on trouve que $L(S^{n}(y))\to 0$. Mais $L(S^{n}(y))$ est une suite constante, d'après \ref{cond ii}, donc $L(S^{n}(y)) = 0$ pour $n\geq0$, en particulier pour $n=0$, \hl{$L(y) = 0$}.

    \item Notons $I$ l'identité de $E$. Comme $I-S$ est linéaire, l'espace $W=\ker(I-S)$ est un sous-espace vectoriel.
    \begin{itemize}
      \item Soit $L$ \hl{une limite de Banach, dont on suppose l'existence}. Nous avons $L(\ind-w)=1$ pour $w \in W$, car $L(w)=0$ d'après \ref{cond ii} et $L(\ind) = 1$ d'après \ref{cond i}. Ainsi $1 = \abs{L(\ind-w)} \leq \norminf{\ind-w}$ car $\normop{L}=1$ d'après \ref{cond ii}. Donc $\dist(\ind, W)\geq 1$, et comme $\norminf{\ind-0}=1$ et $0 \in W$, on trouve \hl{$\dist(\ind, W) = 1$}.

      \item \hl{On suppose $\dist(\ind, W) = 1$}. D'après une des versions du théorème de Hahn-Banach vues en cours, il existe une forme linéaire $L\in E^{*}$ telle que
      \begin{enumerate}
        \item $\hl{\normop{L}=1}$ et $\hl{L(\ind)} = \dist(\ind, W) \hl{= 1}$, donc $L$ vérifie \ref{cond i} ;
        \item $L$ est nulle sur $W$, c.-à-d. $\forall x \in E$, $\hl{L(x-S(x))=0}$, donc $L$ vérifie \ref{cond ii}.
      \end{enumerate}
      Donc la forme ainsi construite \hl{$L$ est une limite de Banach}.
    \end{itemize}

    \item Soit $x=(a,b,a,b,\dots)$. Nous avons \hl{$x = \frac{x+S(x)}{2} + \frac{x-S(x)}{2}$} avec $\hl{\frac{x+S(x)}{2}} = (\frac{a+b}{2}, \frac{a+b}{2}, \dots) \hl{= \frac{a+b}{2}\ind}$. Et comme \hl{$L\left(\frac{x-S(x)}{2}\right) = 0$} par \ref{cond ii}, on trouve $\hl{L(x) =} L\left(\frac{x+S(x)}{2}\right) = \frac{a+b}{2}L(\ind) \overset{\ref{cond i}}{=}\hl{\frac{a+b}{2}}$.

    \item Soient \hl{$L$ une limite de Banach},  $a=(1,0,1,0,\dots)$ et $b=S(a)=(0,1,0,1,\dots)$. Nous avons, d'après la question précédente, $L(a)=L(b)=\frac{1}{2}$ et $L(ab) = 0$, car $ab=(0,0,0,\dots)$. Ainsi $\hl{L(ab)} = 0 \hl{\neq} \frac{1}{4} = \hl{L(a)L(b)}$, donc \hl{$L$ n'est pas multiplicative}.

    \item\label{ex1a} Nous avons $\abs{\frac{x_0 + x_1 + \dots + x_{n-1}}{n}} \leq \frac{n\norminf{x}}{n} = \norminf{x}$. Par passage à la limite on trouve $\abs{\ell(x)} \leq \norminf{x}$ pour tout $x \in E$, donc \hl{$\normop{\ell} \leq 1$}. De plus $\ell(\ind) = \lim_{n\to +\infty} \frac{1 + 1 + \dots + 1}{n} = \lim_{n\to +\infty} 1 = 1$, donc $\abs{\ell(\ind)} = \norminf{\ind}$ et par conséquent \hl{$\normop{\ell} \geq 1$}.

    \item\label{ex1b} Nous avons $x-S(x) = (x_0-x_1,x_1-x_2,\dots,x_{n-1}-x_n,\dots)$ et
    \[
      \lim \frac{(x_0-x_1) + (x_1-x_2) + \dots + (x_{n-1}-x_n)}{n} = \lim \frac{x_0-x_n}{n} = 0,
    \]
    car $\abs{x_0-x_n} \leq 2\norminf{x}$. Ainsi \hl{$x-S(x) \in M$} et \hl{$\ell(x-S(x)) = 0$}.
    \item Comme $E$ est un espace de Banach, $M$ est un sous-espace vectoriel et $\ell \in M^{*}$, une forme linéaire continue sur $M$, d'après le théorème de Hahn-Banach, il existe $L \in E^{*}$ prolongement de $\ell$ tel que $\normop{L} = \normop{\ell} = 1$. Cette forme linéaire $L$ est une \emph{limite de Banach} car elle vérifie les conditions :
    \begin{enumerate}[(i)]
      \item Comme $\ind \in M$, nous avons $\hl{L(\ind)} = \ell(\ind) \hl{= 1}$ d'après \eqref{ex1a}. Et \hl{$\normop{L} = 1$} par la construction de $L$.
      \item D'après \eqref{ex1b}, \hl{pour tout $x \in E$}, $x-S(x) \in M$ et $L(x-S(x)) = \ell(x-S(x)) = 0$, donc \hl{$L(x) = L(S(x))$}.
    \end{enumerate}
    \emph{Remarque : si $x = \suiteN{x_n}$ telle que \hl{$\lim x_{n} = c$}, alors la somme de Cesàro a la même limite $\lim \frac{x_0 + x_1 + \dots + x_{n-1}}{n} = c$. En particulier $x \in M$ et $\hl{L(x)} = \ell(x) \hl{= c}$, ce qui confirme la première question.}
  \end{enumerate}

\end{solution}

\end{document}

