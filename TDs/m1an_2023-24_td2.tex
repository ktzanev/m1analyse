\documentclass[a4paper,12pt,reqno]{amsart}
\usepackage{lille}
\input{m1analyse}

\lilleset{titre=Fiche 2 : Théorèmes fondamentaux d'analyse fonctionnelle}

\begin{document}


% ==================================================
\section{Théorème de Baire}
% ==================================================

% --------------------------------------------------
\begin{exo}[.7]

  On dit qu'un point $x$ d'un espace métrique $X$ est \emph{isolé}, s'il existe $\varepsilon > 0$ tel que $B(x,\varepsilon)=\{x\}$. Montrer qu'un fermé dénombrable de $\mathbb{R}$ a au moins un point isolé.
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $(E,d)$ un espace métrique complet non vide. On dit qu'une partie $A$ de $E$ est $G_\delta$ si $A$ est une intersection dénombrable d'ouverts de $E$.
  \begin{enumerate}
   \item Montrer que si $A$ et $B$ sont deux $G_\delta$ denses, alors $A\cap B$ est encore $G_\delta$ dense.
   \item Montrer que $\mathbb{R}\setminus\mathbb{Q}$ est $G_\delta$ dense. En déduire que $\mathbb{Q}$ ne peut pas s'écrire comme une intersection dénombrable d'ouverts de $\mathbb{R}$
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}
  \begin{enumerate}
    \item Soit $E$ un espace normé complet et $F$ un sous-espace vectoriel de $E$. Montrer que si l'intérieur de $F$ est non-vide alors $F = E$. En déduire que $E$ n'est pas une réunion dénombrable de sous-espaces vectoriels propres fermés.
    \item Montrer qu'un espace de Banach de dimension infinie ne possède pas de \emph{base algébrique} dénombrable. En déduire que l'espace des polynômes $\mathbb{K}[X]$ n'est complet pour aucune norme.
    \item Soit $T : E \to E$ une application linéaire et continue sur l'espace de Banach $E$. On suppose que pour tout $x \in E$ il existe $n = n(x) \ge 1$ tel que $T^nx = 0$. Montrer l'existence d'un nombre entier positif $k$ tel que $T^k = 0$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $(E,d)$ un espace métrique complet, et $(F_n)_n$ une suite de fermés de $E$ telle que $E=\bigcup_nF_n$. Montrer que $\Omega=\bigcup_n\overset{\circ}{F_n}$ est un ouvert dense de $E$.\\
  \indication{on pourra introduire $F'_n=F_n\cap (E\setminus \Omega)$.}
\end{exo}

% --------------------------------------------------
\begin{exo}[.7]

  Soit $U$ un ouvert connexe de $\mathbb{C}$ non vide, $f\in\mathcal{O}(U)$, une fonction holomorphe sur $U$, telle que pour tout $z\in U$, il existe $n\in\mathbb{N}$ tel que $f^{(n)}(z)=0$. Montrer que $f$ est un polynôme.
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $h$ une application définie sur un espace métrique complet $(X,d)$, à valeurs réelles. On suppose que $h$ est semi-continue inférieurement, c.-à-d. pour tout $\alpha\in\mathbb{R}$, $\ensemble{x\in X}{h(x)>\alpha}$ est ouvert.
  \begin{enumerate}
    \item Montrer qu'il existe un ouvert non vide $O$ sur lequel $h$ est majorée.
    \item Application : soit $(f_n)$ une suite de formes linéaires continues sur un Banach $(X,\norm{})$, vérifiant
    \vspace{-7pt}
    \[
      \forall {x\in X},\; \sup_n\vert f_n(x)\vert<\infty.
    \]
    En utilisant ce qui précède, montrer que $ \sup_n\Vert f_n\Vert<\infty$.\\
    \indication{considérer $h: X\to \mathbb{R}$ définie par $h(x)=\sup_n|f_n(x)|$.}
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $f$ une fonction continue sur $\mathbb{R}$ telle que
  \[
    \lim_{n\to\infty}f(nt) = 0
  \]
  pour tout $t\geq 1$. On va montrer que $\lim_{x\to+\infty}f(x) = 0$.
  \begin{enumerate}
    \item Fixons $\varepsilon>0$. En considérant $F_n=\ensemble{t\in[1,+\infty[}{|f(pt)|\leq\varepsilon,\ \forall p\geq n}$, montrer qu'il existe $n_0$ et $\alpha<\beta$ tels que
    \[
      p\geq n_0,\ t\in[\alpha,\beta]\Longrightarrow |f(pt)|\leq \varepsilon.
    \]
    \item On pose $E=\cup_{p\geq n_0}[p\alpha,p\beta]$. Montrer que $E$ contient une demi-droite $[A,+\infty[$; en déduire que $\lim_{x\to+\infty}f(x) = 0$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  On munit $\mathcal{C}([0,1])$ de la norme uniforme. Soient
  \begin{align*}
  F   & =\ensemble[\big]{f\in\mathcal{C}([0,1])}{\exists x\in[0,1],\ f \text{ est dérivable en $x$}},                                                                    \\
  F_n & =\ensemble[\big]{f\in\mathcal{C}([0,1])}{\exists x\in[0,1],\ \forall y\in[0,1],\ x\neq y,\ \left|\tfrac{f(x)-f(y)}{x-y}\right|\leq n},\ n\in\mathbb{N}.
  \end{align*}
  \begin{enumerate}
    \item Soit $f\in\mathcal{C}([0,1])$ dérivable en un point $x\in [0,1]$. Montrer que $\sup_{y\neq x}\left|\frac{f(y)-f(x)}{y-x}\right|$ est fini.
    \item Montrer que pour tout $n$, $F_n$ est fermé et d'intérieur vide.\\
    \indication{on pourra construire $g\in \mathcal{C}([0,1])$ telle que $\norm{g}_\infty$ est arbitrairement petite, mais telle que pour tout $x\in[0,1]$, $\sup_{y\neq x} \left|\frac{g(x)-g(y)}{x-y}\right|$ est arbitrairement grand.}
    \item En déduire que $F$ est d'intérieur vide puis que l'ensemble des fonctions nulle part dérivables est dense dans $\mathcal{C}([0,1])$.
  \end{enumerate}
\end{exo}


% ==================================================
\section{Théorème de Banach-Steinhaus}
% ==================================================

% --------------------------------------------------
\begin{exo}

  Soit $Y=\ell^2(\mathbb{N})$ l'espace des suites réelles $x=(x_n)_n$ telles que $\norm{x}_2=\left(\sum_{n=0}^{+\infty} x^2_n\right)^{\frac12}<+\infty$ et $X$ le sous-espace de $Y$ constitué des suites à support fini. On rappelle que $(Y,\norm{}_2)$ est un espace de Banach. Soit $T_n:X\to Y$ définie sur la suite $x=(x_i)_i$ par
  \[
    (T_n(x))_i =
    \begin{cases}
      0    & \text{si } i\neq n \\
      nx_n & \text{si } i=n
    \end{cases}.
  \]
  Montrer que $\sup_n \norm{T_n(x)}_{2} < \infty$ pour tout $x$, mais que $\sup_n \norm{T_n} = \infty$. Est-ce un contre-exemple au théorème de Banach-Steinhaus ?
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $E$ et $F$ deux espaces de Banach, $(T_n)_n$ une suite dans $\mathcal{L}(E,F)$ tels que la limite $\lim_{n\to\infty} T_n(x)$ existe pour tout $x \in E$.
  \begin{enumerate}
    \item Notons $T(x)=\lim_{n\to\infty} T_n(x)$, $x\in E$. Rappeler pourquoi $T$ est une application linéaire continue.
    \item Montrer que la convergence est uniforme sur tout compact de $E$. L'est-elle sur tout fermé borné ?
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $D$ une partie d'un espace vectoriel normé $E$. On suppose que pour toute forme linéaire continue $\xi \in E'$ l'ensemble $\xi(D)$ est borné. Montrer que $D$ est une partie bornée de $E$.\\
  \indication{montrer, en utilisant Hahn-Banach, que l'inclusion naturelle de $E$ dans son bidual $E''$ est une isométrie (exercice \ref{forme-norme}).}
\end{exo}


% ==================================================
\section{Théorème de Banach-Schauder}
% ==================================================

% --------------------------------------------------
\begin{exo}
  \begin{enumerate}
    \item Soit $c_0$ l'espace des suites complexes convergentes vers zéro, muni de la norme uniforme, c.-à-d. $\norm{x} _\infty = \sup _n |x_n|$. On considère l'application linéaire $T:c_0 \to c_0$ définie par
    \vspace{-3pt}
    \[
      T(x_1,x_2,...,x_n,...) = (x_1, x_2/2,..., x_n/n,...).
      \vspace{-11pt}
    \]
    Montrer que
    \begin{enumerate}
      \item $T$ est continue et injective; calculer $\norm{T}$ ;
      \item $T$ est à image dense ;
      \item $T$ n'est pas ouverte ;
      \item $T$ n'est pas un homéomorphisme sur son image.
    \end{enumerate}
    \item Lesquelles des propriétés restent vraies si on remplace $c_0$ par $l^\infty$ ?
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soient $E$ et $F$ deux espaces de Banach, $T\in \mathcal{L}(E,F)$.
  \begin{enumerate}
    \item Soit $0<\alpha<1$. On dit que $T$ est $\alpha$-ouverte si
    \[
      \exists C>0;\ \forall y\in F, \norm{y} \leq 1,\exists x\in E;\ \norm{x}\leq C\ \text{et}\ \norm{y-Tx} < \alpha .
    \]
    Montrer qu'une application $\alpha$-ouverte est ouverte.
    \item Montrer que l'ensemble des surjections linéaires continues de $E$ sur $F$ (noté ${\mathcal{S}}(E,F)$) est ouvert dans ${\mathcal{B}}(E,F)$
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $(E,\vert \cdot \vert)$ un espace de Banach. Soient $F_{1}$ et $F_{2}$ deux sous-espaces vectoriels fermés de $E$. On suppose que la somme $F_{1}+F_{2}=\ensemble{x+y}{x\in F_{1},y\in F_{2}}$ est fermée.
  \begin{enumerate}
    \item Vérifier que $F_1\times F_2$ muni de la norme $\norm{(x_1,x_2)}_1=\vert x_1\vert +\vert x_2\vert$ est complet.
    \item Que peut-on dire de l'application $\varphi:(x_1,x_2)\mapsto x_1+x_2$ sur $F_1\times F_2$?
    \item En déduire qu'il existe une constante $C$ telle que pour tout $z$ de $F_{1}+F_{2}$, il existe $x_{1}$ dans $F_{1}$ et $x_{2}$ dans $F_{2}$ avec : $z=x_{1}+x_{2}$ et $\vert x_{1}\vert \le C\vert z \vert$, $\vert x_{2}\vert \le C \vert z\vert$.
    \item \emph{(application)} Soit $X$ un espace de Banach et $P : X \to X$ une application linéaire telle que $P^2 = P$. On suppose que l'image de $P$ et le noyau de $P$ sont des sous-espaces fermés. Montrer que $P$ est continue.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  On note $E$ l'espace des fonctions continues de $I=[0;1]$ dans $\mathbb{R}$, muni de la norme $\norm{f}_\infty =\sup_{x\in I}\norm{f(x)}$. Soit $G$ un sous-espace vectoriel de $E$. On suppose que $G$ est fermé dans $(E,\norm{}_\infty)$, et contenu dans $F=\mathcal{C}^{1}(I,\mathbb{R})$. Le but est de montrer qu'alors $G$ est de dimension finie.
  \begin{enumerate}
    \item Montrer que $F$ n'est pas fermé dans $E$.
    \item Pour $f\in F$, on pose $\norm{f}=\norm{f}_\infty+\norm{f'}_\infty$. Les normes $\norm{}_\infty$ et $\norm{}$ sont-elles équivalentes sur $F$ ?
    \item Montrer que $G$ est fermé dans $(F,\norm{})$.
    \item Montrer que $\norm{}_\infty$ et $\norm{}$ sont équivalentes sur $G$ à l'aide du théorème d'isomorphisme de Banach.
    \item En utilisant le théorème d'Ascoli, montrer que la boule unité fermée de $G$ pour $\norm{}_\infty$ est un compact de
    $(E,\norm{}_\infty)$.
    \item Conclure à l'aide du théorème de Riesz \emph{(la boule unité fermée d'un e.v.n est compacte si et seulement s'il est de dimension finie)}.
  \end{enumerate}
\end{exo}


% ==================================================
\section{Théorème du graphe fermé}
% ==================================================

% --------------------------------------------------
\begin{exo}

  Soient $E$ et $F$ deux espaces de Banach, et $G= \ell_{\infty}(F)$ l'espace vectoriel des suites bornées
  \[
    x = (x_n)_{n\in \mathbb{N}}, \quad x_n \in F,
  \]
  muni de la norme sup : $\norm{x}_{\infty}=\sup_n\norm{x_n}_F$.
  \begin{enumerate}
    \item Montrer que $G$ est un espace de Banach.
    \item Soit $(T_n)$ une suite d'applications linéaires continues de $E$ dans $F$, telles que
    \[
    \sup_n \big(\norm{T_nx}_F\big) < \infty
    \]
    pour tout $x \in E$. On note $T : E \to G$, définie par $T(x) = \suiteN{T_nx}$. Montrer que le graphe de $T$ est fermé.
    \item En déduire que le théorème de Banach-Steinhaus, dans le cas où les deux espaces $E$ et $F$ sont des espaces de Banach, peut être établi comme une conséquence du théorème du graphe fermé.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soient $E$ et $F$ deux espaces de Banach et $T:E\to F$ une application linéaire. Monter que $T$ est continue si et seulement si pour toute forme linéaire continue $\xi \in F'$, la forme linéaire $\xi\circ T :E\to \mathbb{R}$ est continue.\\
  \indication{en utilisant Hahn-Banach, exercice \ref{forme-norme}, montrer que $\ensemble[\big]{(x,y)\in E \times F}{\forall \xi \in F', \xi(y)=\xi(T(x))}$ est le graphe de $T$.}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $E=\mathcal{C}([0,1])$, et $\norm{}$ une norme sur $E$ pour laquelle $E$ est un Banach. Supposons que, pour toute suite $\suite{f_n}$ de $E$ qui converge vers $f\in E$ pour la norme $\norm{}$ et tout $t\in[0,1]$, on ait $\lim_{n\to +\infty} f_n(t)=f(t)$. Montrer que les normes $\norm{}$ et $\norm{}_\infty$ sont équivalentes.
\end{exo}


% ==================================================
\section{Théorème de Hahn-Banach}
% ==================================================

% --------------------------------------------------
\begin{exo}[.7]\label{forme-norme}

  Soit $(E,\norm{})$ un espace normé et $x\in E$.
  \begin{enumerate}
    \item Montrer que $\norm{x}=\sup\ensemble{|\lambda(x)|}{\lambda \in E',\ \vertiii{\lambda}=1}$ et que la borne supérieure est atteinte.
    \item Montrer que l’inclusion naturelle de $E$ dans son bidual $E''$ est une isométrie.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $p\in[1,+\infty[$, $\mathbb{D}=\ensemble{z\in\mathbb{C}}{|z|<1}$. Pour $\lambda\in\mathbb{D}$, on note $x_\lambda$ la suite $x_\lambda=(\lambda^n)_{n\in\mathbb{N}}$.
  \begin{enumerate}
    \item Montrer que pour tout $\lambda\in\mathbb{D}$, $x_\lambda$ appartient à $\ell^p(\mathbb{N})$.
  \end{enumerate}
  Soit $\Lambda\subset\mathbb{D}$. On suppose que $\Lambda$ a un point d'accumulation dans $\mathbb{D}$.
  \begin{enumerate}[resume]
    \item Soit $\varphi$ une forme linéaire continue sur $\ell^p(\mathbb{N})$ telle que $\varphi(x_\lambda)=0$ quel que soit $\lambda\in \Lambda$.
    \begin{enumerate}
      \item Soit $f:\mathbb{D}\to\mathbb{C}$ définie par $f(z)=\sum_{k=0}^{+\infty} z^k \varphi(e_k)$ où $e_k$ est la suite dont tous les termes sont nuls excepté le $k$-ième qui vaut 1. Montrer que $f$ est holomorphe sur $\mathbb{D}$.
      \item Montrer que $f(\lambda)=0$ pour tout $\lambda\in \Lambda$.
      \item En déduire que $\varphi(e_k)=0$ pour tout $k$, puis que $\varphi$ s'annule sur $\ell^p(\mathbb{N})$.
    \end{enumerate}
    \item Montrer que $\mathrm{vect}\{x_\lambda,\ \lambda\in\Lambda\}$ est dense dans $\ell^p(\mathbb{N})$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $(E,\norm{})$ un espace normé (sur $\mathbb{R}$ ou $\mathbb{C}$) tel que $E'$, le dual topologique de $E$, est séparable. Le but de cet exercice est de montrer que $E$ est séparable.
  \begin{enumerate}
    \item Montrer qu'il existe une suite $(\lambda_n)_n$ d'éléments de $E'$ de norme $1$, dense dans $\Sigma =\{\lambda\in E',\ \norm{\lambda}_{E'}=1\}$.
    \item Montrer que pour tout $n\in\mathbb{N}$, il existe $x_n\in E$ tel que $\norm{x_n}=1$ et $|\lambda_n(x_n)|\geq \frac12$.
    \item Montrer que $V=\mathrm{vect}\{x_n,\ n\in\mathbb{N}\}$ est dense dans $E$. En déduire que $E$ est séparable.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $X$ un espace vectoriel réel. On dit que $p:X\to \mathbb{R}$ est sous linéaire si :
  \begin{enumerate}[(i)]
    \item Pour tout $x,y\in X,$ $p(x+y)\leq p(x)+p(y)$.
    \item Pour tout $x\in X$ et tout $\lambda\geq 0$, $p(\lambda x)=\lambda p(x)$.
  \end{enumerate}
  \begin{enumerate}
    \item Soit $p_1,\ldots, p_n:X\to\mathbb{R}$ des applications sous-linéaires. Soit également $L:X\to\mathbb{R}$ une forme linéaire. On suppose qu'on a
    \[
      \forall x\in X, \quad L(x)\leq \sum_{k=1}^n p_k(x).
    \]
    En considérant $E=\ensemble{(x,\ldots,x)}{x\in X}\subset X^n$ et $p:X^n\to\mathbb{R}$ définie par $p(x_1,\ldots, x_n)\coloneqq\sum_{k=1}^n p_k(x_k)$, montrer qu'il existe des formes linéaires $L_1,\ldots, L_n:X\to\mathbb{R}$ telles que
    \[
      L=\sum_{k=1}^n L_k\quad\text{et}\quad L_k\leq p_k,\ \forall k\in\{1,\ldots,n\}.
    \]
    \item \emph{(application)} On dit qu'une forme linéaire $L:\mathcal{C}([0,1],\mathbb{R})\to\mathbb{R}$ est positive si on a $L(f)\geq 0$ pour toute fonction $f\geq 0$. Montrer que toute forme linéaire continue $L$ sur $\mathcal{C}([0,1],\mathbb{R})$ peut s'écrire $L=L_1-L_2$ avec $L_1$ et $L_2$ des formes linéaires continues positives.\\
    \indication{se ramener au cas $\vertiii{L}=1$ et considérer $p_1(f)\coloneqq\norm{f^+}_\infty$ et $p_2(f)=\norm{f^-}_\infty$, où $f^+ = \max\{f,0\}$ et $f^- = \min\{f,0\}$.}
  \end{enumerate}
\end{exo}

\end{document}
