\documentclass[a4paper,12pt,reqno]{amsart}
\usepackage{lille}
\input{m1analyse}

\lilleset{titre=Fiche 1 : espaces de fonctions}

\begin{document}


% ==================================================
\section{Espaces normés}
% ==================================================

% --------------------------------------------------
\begin{exo}

  Soit $E$ un espace vectoriel sur $\mathbb{K}$, $\mathbb{K}=\mathbb{R}$ ou $\mathbb{C}$, et $d$ une distance sur $E$ telle que
  \begin{enumerate}[(i)]
    \item $d$ est invariante par translations, c.-à-d. $d(x+z,y+z) = d(x,y),\;\forall x,y,z \in E$,
    \item $d$ est absolument homogène, c.-à-d. $d(\lambda x, \lambda y) = |\lambda| d(x,y),\;\forall x,y \in E, \forall \lambda \in \mathbb{K}$.
  \end{enumerate}

  \begin{enumerate}
    \item Montrer qu'il existe une norme $N$ sur $E$ telle que $d(x,y) = N(x-y)$.
    \item Donner un exemple d'une métrique $d$ sur $\mathbb{R}$ qui n'est pas invariante par translations.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}
  \begin{enumerate}
    \item Soit $E=\{0\}\cup[1,+\infty[$ muni de la métrique $d$ définie par $d(x,y)=|x-y|$. Montrer $\overline{B(0,1)}\neq \ensemble{x\in E}{d(0,x)\leq 1}$.
    \item Soit $(E,\norm{})$ un espace vectoriel normé. Montrer que pour tout $r>0$ et tout $x\in E$, $\overline{B(x,r)}= \ensemble{y\in E}{\norm{x-y}\leq r}$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $(E,\norm{})$ un espace normé.
  \begin{enumerate}
    \item Montrer que pour tout $x\in E$,
    \[
      \norm{x}=\inf\ensemble{t>0}{x\in tB(0,1)}.
    \]
    \item Montrer que pour tout $x,y\in B(0,1)$, tout $\lambda,\mu\in\mathbb{R}$ tels que $|\lambda|+|\mu|\leq 1$, $\lambda x+\mu y$ appartient à $B(0,1)$.
    \item Pour tout $x\in B(0,1)$, il existe $\varepsilon>0$ tel que $x+\varepsilon B(0,1)\subset B(0,1)$.
    \item Pour tout $x\in E$, $x\neq 0$, il existe $\lambda,\mu\in\mathbb{R}^*$ tel que $\lambda x$ appartienne à $B(0,1)$ et $\mu x$ n'appartienne pas à $B(0,1)$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}[.7]

  Montrer que pour tout entier $n\geq 1$, il existe $\alpha_n>0$ tel que pour tout $t\in[0,1]$ et tout polynôme $P$ de degré inférieur ou égal à $n$, on a
  \[
    |P(t)|\leq \alpha_n \int_0^1|P(x)|dx.
  \]
\end{exo}


% ==================================================
\section{Espaces de Banach}
% ==================================================

% --------------------------------------------------
\begin{exo}

  Soit $(E,d)$ un espace métrique et $\suite{x_n}$ une suite dans $E$.
  \begin{enumerate}
    \item Montrer que si $\suite{x_n}$ est une suite de Cauchy, alors il existe une sous-suite $\suite[k]{y_k}=\suite[k]{x_{n_k}}$ telle que $\sum_{k\geq 0} d(y_{k+1},y_k)$ converge.
    \item Montrer que si $\suite{x_n}$ est une suite de Cauchy, alors elle converge si et seulement si elle admet une sous-suite convergente.
    \item En déduire que $(E,d)$ est complet si et seulement si toute suite $\suite{x_n}$ de $E$ telle que $\sum_{n=0}^{+\infty} d(x_{n+1},x_n)<\infty$ converge.
    \item Montrer que si $(E,d)$ est un espace métrique compact alors il est complet.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}
  \begin{enumerate}
  \item Montrer qu'un espace normé est complet si et seulement si toute série absolument convergente est convergente.
  \item Soit $E=\mathcal{C}([-1,1])$, l'espace des fonctions continues sur $[-1,1]$ muni de la norme $\norm{}_1$ définie pour $f\in E$ par $\norm{f}_1=\int_{-1}^1|f(t)|dt$. Pour $n\in\mathbb{N}^*$, soit
  \[
    f_n(t)=
    \begin{cases}
      0  &\text{si } t\in[-1,0]\\
      nt &\text{si } t\in\left[0,\frac1n\right]\\
      1  &\text{si } t\in\left[\frac1n,1\right]
    \end{cases}.
  \]
  \begin{enumerate}
    \item Montrer que $\suite{f_n }$ est de Cauchy dans $E$. Est-elle convergente dans $E$ ?
    \item On considère la série de terme général $g_n=f_{n+1}-f_n$. Montrer que $\sum_{n=1}^{+\infty}\norm{g_n}_1<+\infty$ mais que la série $\sum_{n\geq 1} g_n$ ne converge pas dans $E$.
  \end{enumerate}
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $E$ un espace vectoriel normé et $M$ un sous-espace vectoriel de $E$. On note $E/M$ l'espace quotient de $E$ par la relation $x\sim y \Leftrightarrow x-y\in M$. Pour $\overline{x} \in E/M$, on pose
  \[
    N(\overline{x})=d(x,M)=\inf\ensemble{\norm{x-y}}{y\in M}.
  \]
  \begin{enumerate}
    \item
    \begin{enumerate}
      \item Montrer que $N(\overline{x})=\inf\ensemble{\norm{z}}{z\in\overline{x}}$.
      \item Montrer que $N$ est une semi-norme sur $E/M$ (c.-à-d. $N$ vérifie l'inégalité triangulaire et l'hypothèse d'homogénéité, mais n'est pas définie positive).
      \item Déterminer les $x \in E$ tels que $N(\overline{x})=0$. En déduire que $N$ est une norme sur $E/M$ si et seulement si $M$ est fermé.
    \end{enumerate}
  \end{enumerate}
  \emph{On suppose pour la suite que $M$ est fermé.}
  \begin{enumerate}[resume]
    \item Montrer que si $E$ est complet alors $E/M$ est complet.
    \item Montrer que si $M$ et $E/M$ sont complets alors $E$ est complet.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $\alpha=\suite[n\in\mathbb{N}]{\alpha_n}$ une suite de nombres strictement positifs et $1\leq p<+\infty$. On note
  \begin{align*}
    \ell^p_\alpha=\ensemble{x=\suite{x_n}}{x_n\in\mathbb{C}\text{ et } \sum_{n=1}^\infty \alpha_n |x_n|^p <\infty},\\
    \ell^\infty_\alpha=\ensemble{x=\suite{x_n}}{x_n\in\mathbb{C}\text{ et } \sup_{n\geq 1}(\alpha_n |x_n|) <\infty}.
  \end{align*}
  Les ensembles $\ell^p_\alpha$ et $\ell^\infty_\alpha$ sont munis respectivement des normes $\norm{x}_{p,\alpha}=\left(\sum_{n=1}^\infty \alpha_n |x_n|^p\right)^{\frac1p}$ et $\norm{x}_{p,\infty}= \sup_{n\geq 1}( \alpha_n |x_n|)$.

  Pour $p\in[1,++\infty]$, on note $p'$ l'élément de $[1,+\infty]$ tel que $\frac1{p'}+\frac1p=1$.
  \begin{enumerate}
  \item
  \begin{enumerate}
    \item Pour $\alpha,\beta\in\mathbb{R}^+,$ et $p>1$, montrer que $\alpha\beta\leq \frac1p\alpha^p+\frac1{p'} \beta^{{p'}}$.
    \item Montrer que pour tout $1< p< +\infty$, tout $x\in\ell^p_\alpha$ et tout $y\in\ell^{p'}_\alpha$, $xy=\suite{x_ny_n}$ appartient à $\ell^1_\alpha$ et
    $$\norm{xy}_{1,\alpha}\leq \norm{x}_{p,\alpha}\norm{y}_{p',\alpha}\quad \text{(inégalité de Hölder).}$$
    \item Démontrer que pour tout $p\in [1,+\infty]$ et pour tout $x,y\in\ell^p_\alpha$, $x+y=\suite{x_n+y_n}$ appartient à $\ell^p_\alpha$ et
    $$\norm{x+y}_{p,\alpha}\leq \norm{x}_{p,\alpha}+\norm{y}_{p,\alpha} \quad \text{(inégalité de Minkowski).}$$
    \item En déduire que pour tout $p\in[1,+\infty]$, $(\ell^p_\alpha,\norm{}_{p,\alpha})$ est un espace vectoriel normé.
  \end{enumerate}
  \item Montrer que $\ell^p_\alpha$ et $\ell^\infty_\alpha$ sont des espaces de Banach.
  \item On choisit $\alpha_n=1$ pour tout $n\geq 1$. Montrer que si $1\leq p<q\leq \infty$, alors $\ell_1^p$ est strictement inclus dans $\ell^q_1$ et que pour tout $x\in\ell_1$, $\norm{x}_{q,1}\leq \norm{x}_{p,1}$.
  \item Soit $c_{0,\alpha}=\{x=\suite{x_n}\mid x_n\in\mathbb{C}\text{ et } \lim_{n\to +\infty} \alpha_n x_n =0\}.$ Montrer que $c_{0,\alpha}$ muni de la norme $\norm{x}_{\infty,\alpha}$ est un espace de Banach.
  \end{enumerate}
\end{exo}


% ==================================================
\section{Stone-Weierstrass}
% ==================================================

% --------------------------------------------------
\begin{exo}[0.7]

  Soit $(K,d)$ un espace métrique compact, et $\{f_1,\dots,f_d\}$ une famille de $\mathcal{C}(K,\mathbb{R})$ qui sépare les points de $K$. Montrer que $\phi=(f_1,\dots,f_d)$ est continue et injective sur $K$. En déduire que $K$ est homéomorphe à une partie de $\mathbb{R}^d$.
\end{exo}

% --------------------------------------------------
\begin{exo}
  \begin{enumerate}
  \item Soit $\varphi:[0,1]\to\mathbb{R}$ une fonction continue strictement monotone.\\
  Montrer que $\mathcal{A_\varphi}=\ensemble{P\circ\varphi}{P\ \text{polynomiale}}$ est dense dans $\mathcal{C}([0,1],\mathbb{R})$ pour $\norm{}_\infty$. En déduire que l'ensemble
  \[
    \ensemble{f:[0,1]\to\mathbb{R}}{f(x)=\sum_{k=0}^na_k\sin^k(x),\ a_k\in\mathbb{R},\ n\in\mathbb{N}}
  \]
  est dense dans $\mathcal{C}([0,1],\mathbb{R})$ pour $\norm{}_\infty$.
  \item Soit $\mathcal{P}$ la sous-algèbre engendrée par les deux éléments $f_0$ et $f_2$, où $f_0(x)=1$ et $f_2(x)=x^2$. Montrer que $\mathcal{P}$ est dense dans $\mathcal{C}([0,1],\mathbb{R})$, mais pas dans $\mathcal{C}([-1;1],\mathbb{R})$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}
  \begin{enumerate}
    \item Soit $f,g\in\mathcal{C}([0,1])$. Montrer que
    \[
      \forall n\in\mathbb{N},\ \int_0^1f(t)t^n\,\mathrm{d}t=\int_0^1g(t)t^n\,\mathrm{d}t \Longleftrightarrow f=g
    \]
    \item Soit $f:[0,+\infty[\to\mathbb{R}$ une fonction continue et bornée.
    \begin{enumerate}
      \item Montrer qu'il existe $g\in\mathcal{C}([0,1])$ telle que pour tout $n\geq 2$
      \[
        \int_0^{+\infty}f(x)e^{-nx}\mathrm{d}x=\int_0^1g(t)t^{n-2}\,\mathrm{d}t.
      \]
      \item En déduire que si, pour tout $n\geq 2$, $\int_0^{+\infty} f(x)e^{-nx}dx=0$ alors $f=0$.
    \end{enumerate}
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}[.49]

  Soit $f$ une fonction continue sur un intervalle $I$ non borné de $\mathbb{R}$. Montrer que $f$ est limite uniforme d'une suite de polynômes $\suite{P_n}$ sur $I$ si et seulement si $f$ est un polynôme.
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $f:]0,1[\to\mathbb{R}$ une fonction continue. Montrer que $f$ est une limite uniforme de polynôme sur $]0,1[$ si et seulement si $f$ admet une extension continue sur $[0,1]$.
\end{exo}

% --------------------------------------------------
\begin{exo}\textbf{(Polynômes de Bernstein)}

  Pour $n\in\mathbb{N}$ et $k\in\{0,1,\ldots, n\}$, on pose
  \[
    B_{n,k}(x)=\binom n k x^k(1-x)^{n-k}.
  \]
  où $\binom n k=\frac{n!}{k!(n-k)!}$ est le coefficient binomial.
  \begin{enumerate}
    \item Calculer $\sum_{k=0}^n B_{n,k}(x),\ \sum_{k=0}^n kB_{n,k}(x) \text{ et } \sum_{k=0}^n k^2B_{n,k}(x).$
    \item En déduire que $\sum_{k=0}^n (k-nx)^2 B_{n,k}(x)=nx(1-x)$.
    \item Soit $\alpha>0$ et $x\in[0,1]$. On pose
    $A=\ensemble[\big]{k\in \{0,\ldots, n\}}{\left|\frac kn-x\right|\geq \alpha}.$
    Montrer que $\sum_{k\in A} B_{n,k}(x) \leq \frac1{4n\alpha^2}.$
    \item Soit $f:[0,1]\to\mathbb{R}$ continue. On pose
    $f_n(x)=\sum_{k=0}^n f\left( \frac kn\right)B_{n,k}(x).$
    Montrer que $\suite{f_n}$ converge uniformément vers $f$ sur $[0,1]$.
    \item Retrouver le théorème de Weierstrass : Soit $-\infty<a<b<+\infty$. Toute fonction continue sur $[a,b]$ est limite uniforme d'une suite de polynômes.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $(K,d)$ un espace métrique compact, et $\mathcal{A}$ l'ensemble des fonctions lipschitziennes de $K$ dans $\mathbb{R}$.
  \begin{enumerate}
    \item Montrer que $\mathcal{A}$ est dense dans $\mathcal{C}(K,\mathbb{R})$ muni de la norme uniforme.
    \item Pour $f\in\mathcal{C}(K,\mathbb{R})$ et $\lambda>0$, on pose $f_\lambda(x)=\inf_{y\in K}\{f(y)+\lambda d(x,y)\}$.
    Montrer que $f_\lambda$ est lipschitzienne.
    \item Montrer que $\norm{f-f_\lambda}_\infty\xrightarrow[\lambda\to+\infty]{}0$ et retrouver le résultat de la première question. \emph{On pourra appliquer le théorème de Dini.}
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  On note $\mathbb{T}$ le cercle unité du plan complexe.
  \begin{enumerate}
    \item Montrer que $\ensemble{\sum_{k=-n}^n \alpha_k z^k}{n\in\mathbb{N} \text{ et } \forall k, \alpha_k\in\mathbb{C}}$ est dense dans $\mathcal{C}(\mathbb{T},\mathbb{C})$, l'ensemble des fonctions complexes continues sur $\mathbb{T}$.
    \item Montrer que si $f:\mathbb{R}\to\mathbb{C}$ est continue et $2\pi$-périodique, alors $f$ est limite uniforme d'une suite de polynômes trigonométrique.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soient $X$ et $Y$ deux espaces métriques compacts. Soit $\mathcal{A}$ l'ensemble des fonctions $f\in \mathcal{C}(X\times Y,\mathbb{R})$ de la forme:
  \[
   f(x,y)=\sum_{i\in I} u_i(x)v_i(y),\text{ avec } u_i\in \mathcal{C}(X,\mathbb{R}),\ v_i\in \mathcal{C}(Y,\mathbb{R}),\ I \text{ fini}.
  \]
  Montrer que toute fonction de $\mathcal{C}(X\times Y,\mathbb{R})$ est limite uniforme de suites d'éléments de $\mathcal{A}$.
\end{exo}


% ==================================================
\section{Espaces séparables}
% ==================================================

% --------------------------------------------------
\begin{exo}[.35]

  Montrer que tout espace vectoriel normé sur $\mathbb{K}=\mathbb{R}$ ou $\mathbb{C}$ de dimension finie est séparable.
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $E$ un $\mathbb{K}$-espace vectoriel normé ($\mathbb{K}=\mathbb{R}$ ou $\mathbb{C}$). Soit $\suite{x_n}$ une suite de vecteurs de $E$ et notons $F$ le sous-espace engendré par $\suite{x_n}$. Supposons que $F$ est dense dans $E$. Montrer que $E$ est séparable.
\end{exo}

% --------------------------------------------------
\begin{exo}

  \begin{enumerate}
    \item Montrer qu'un espace métrique est séparable si et seulement s'il possède une base de voisinages dénombrable.
    \item Montrer que toute partie d'un espace métrique séparable est aussi (métrique) séparable.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}
  On note $\mathbb{Q}\cap [0,1]=\{a_0=0,a_1,\dots,a_n,\dots\}$ et on pose $f_n(x)=|x-a_n|$.
  \begin{enumerate}
    \item Montrer que la sous-algèbre $\mathcal{A}$ engendrée par les $f_n\ (n\in\mathbb{N})$ est séparante. En déduire que $\mathcal{A}$ est dense dans $\mathcal{C}([0,1],\mathbb{R})$ pour la norme uniforme.
    \item Montrer que tout élément de $\mathcal{A}$ est limite uniforme de combinaisons linéaires à coefficients rationnels de fonctions du type $f_{i_1}\times\cdots\times f_{i_s}$.
    \item En déduire que $\mathcal{C}([0,1],\mathbb{R})$ est séparable.
  \end{enumerate}
\end{exo}


% ==================================================
\section{Équicontinuité}
% ==================================================

% --------------------------------------------------
\begin{exo}

  Montrer que les deux premières familles suivantes sont équicontinues, mais pas la troisième:
  \begin{enumerate}
    \item à $k>0$ fixé, l'ensemble des fonctions $f$ différentiables de $[a;b]$ dans $\mathbb{R}$ telles que $\forall t\in]a;b[,\ |f'(t)|\le k$;
    \item l'ensemble des fonctions $f_n:t\mapsto\sin\!\Big(\sqrt{t+4(n\pi)^2}\Big)$ sur $[0;+\infty[$, $n\in\mathbb{N}$ \emph{(vers quoi cette suite de fonctions converge-t-elle simplement ?)};
    \item l'ensemble des fonctions $f_n:t\mapsto t^n$ sur $[0,1]$, $n\in\mathbb{N}$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}[.7]

  Soit $f\in \mathcal{C}([0,+\infty[)$ et soit $f_n$ la fonction définie pour $x\in[0,+\infty[$ et $n\geq 1$ par $f_n(x)=f(x^n)$. Montrer que $H=\{f_1,f_2,\ldots\}$ est équicontinue en $x=1$ si et seulement si $f$ est constante.
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soient $E$ et $F$ des espaces normés et $\suite{f_n}$ une suite d'applications de $E$ dans $F$ équicontinue en un point $a\in E$. Montrer que si la suite $\suite{f_n(a)}$ converge vers $b\in F$, alors pour toute suite $\suite{x_n}$ qui converge vers $a$, $\suite{f_n(x_n)}$ converge vers $b$. L'équicontinuité est-elle nécessaire ?
\end{exo}


% ==================================================
\section{Ascoli}
% ==================================================

% --------------------------------------------------
\begin{exo}

  Soit $K$ une partie compacte de $\mathbb{R}^n$. On munit les trois espaces $\mathcal{C}(K,\mathbb{R}^n)$, $\mathcal{C}(K,K)$ et
  \[
    \mathcal{I}(K)=\ensemble{f\in\mathcal{C}(K,K)}{\forall x,y,\ \norm{x-y}=\norm{f(x)-f(y)}}
  \]
  de la norme uniforme.
  Montrer que $\mathcal{I}(K)$ est fermé dans $\mathcal{C}(K,K)$, que $\mathcal{C}(K,K)$ est fermé dans $\mathcal{C}(K,\mathbb{R}^n)$ et que $\mathcal{I}(K)$ est compact.
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $(X,d)$ un espace métrique compact et $(Y,\norm{})$ un espace normé. Montrer le sens \enquote{facile} du théorème d'Ascoli dans ce cadre, à savoir qu'une partie compacte de $(\mathcal{C}(X,Y),\norm{}_\infty)$ est nécessairement fermée, bornée et équicontinue.
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $E$ un sous-espace vectoriel de $\mathcal{C}^1([0,1],\mathbb{R})$, muni de la norme uniforme. On suppose qu'il existe $m\in\mathbb{R}$ tel que $\forall f\in E,\ \norm{f'}_\infty\le m\norm{f}_\infty$. On note $\overline{E}$ l'adhérence de $E$ dans $\mathcal{C}^0([0,1],\mathbb{R})$, muni de la norme uniforme.
  \begin{enumerate}
    \item Justifier que $\overline{E}$ est un sous-espace vectoriel de $\mathcal{C}^1([0,1],\mathbb{R})$ qui vérifie la même condition que $E$, $\forall f\in \overline{E},\ \norm{f'}_\infty\le m\norm{f}_\infty$.
    \item Montrer que $B$, la boule unité fermée de $\overline{E}$, est compacte.\\
    \emph{D'après le théorème de Riesz, cela implique que $\overline{E}$ et donc $E$ est de dimension finie $d$ (et par suite que $E$ est fermé !).}
    \item Trouver $s\in\mathbb{N}$ et $x_1,\dots,x_s\in[0,1]$ en fonction de $m$ tels que $f\mapsto(f(x_1),\dots,f(x_s))$ soit injective sur $E$. En déduire qu'on peut contrôler $d$ à l'aide de $m$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soient $(E,d)$ un espace métrique et $\mathcal{H}$ une famille équicontinue d'application de $E$ dans $\mathbb{R}$.
  \begin{enumerate}
    \item Montrer que l'ensemble $A=\{x\in E\ |\ \mathcal{H}(x)\text{ est borné}\}$ est ouvert et fermé.
    \item En déduire que si $E$ est compact et connexe et s'il existe $x_0\in E$ tel que $\mathcal{H}(x_0)$ soit borné, alors $\mathcal{H}$ est relativement compact dans $\mathcal{C}(E,\mathbb{R})$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}\textbf{(Fonctions höldériennes)}

  Pour $\alpha>0$, on pose
  $E_\alpha:=\{f:[0,1]\to\mathbb{C} \mid \exists C\geq 0 \text{ tel que } |f(x)-f(y)|\leq C|x-y|^\alpha,\ \forall x,y\in[0,1]\}.$
  Pour $f\in E_\alpha$, on pose $C_\alpha(f)=\inf\{C\geq 0\ / |f(x)-f(y)|\leq C|x-y|^\alpha,\ \forall x,y\in[0,1]\}.$
  \begin{enumerate}
  \item
  \begin{enumerate}
    \item Montrer que pour tout $x,y\in[0,1]$, $|f(x)-f(y)|\leq C_\alpha(f)|x-y|^\alpha$.
    \item Montrer que $E_\alpha$ est un sous-espace vectoriel de $\mathcal{C}([0,1])$.
    \item Montrer que $f\mapsto C_\alpha(f)$ est une semi-norme sur $E_\alpha$. Déterminer le noyau de $C_\alpha$, c.-à-d. $\{f\in E_\alpha\mid C_\alpha(f)=0\}$.
  \end{enumerate}
  \item Déterminer toutes les fonctions de $E_\alpha$ lorsque $\alpha>1$. On supposera désormais $0<\alpha\leq 1$.
  \item Soit $0<\beta<\alpha\leq 1$. Montrer que $\mathcal {C}^1([0,1])\subset E_\alpha\subset E_\beta \subset \mathcal {C}([0,1])$ et que ces inclusions sont strictes.
  \item Pour $f\in E_\alpha$, on pose $N_\alpha(f)=|f(0)|+C_\alpha(f)$. Montrer que $N_\alpha$ est une norme sur $E_\alpha$ vérifiant $\norm{f}_\infty \leq N_\alpha(f)$ pour tout $f\in E_\alpha$.
  \item Montrer que $(E_\alpha,N_\alpha)$ est complet.
  \item Montrer que la boule unité de $E_\alpha$ est relativement compacte dans $\mathcal{C}([0,1])$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $K:\mathcal{C}([a,b])\to \mathcal{C}([a,b])$ donné par
  \[
    Kf(s)=\int_a^bk(s,t)f(t)dt
  \]
  où $k$ est une fonction continue sur $[a,b]\times[a,b]$. Soit $\suite{f_n}$ une suite bornée de $X=(\mathcal{C}([a,b]),\norm{}_\infty)$.
  \begin{enumerate}
  \item Justifier l'uniforme continuité de $k$.
  \item En déduire que la suite $(Kf_n)_n$ est équicontinue.
  \item Montrer que $(Kf_n)_n$ contient une sous-suite convergente dans $X$.
  \end{enumerate}
\end{exo}

\end{document}
