\documentclass[a4paper,12pt,reqno]{amsart}
\usepackage{lille}
\input{m1analyse}

\lilleset{titre=Fiche 4 : Espaces $L^p$}

\begin{document}


% ==================================================
\section{Relations entre les espaces $L^{p}$}
% ==================================================

% --------------------------------------------------
\begin{exo}

  Donner des exemples montrant que:
  \begin{enumerate}
    \item il n'y a aucune inclusion entre $L^\infty(]0;+\infty[)$ et $L^1(]0;+\infty[)$;
    \item il n'y a aucune inclusion entre $L^1(]0;+\infty[)$ et $L^2(]0;+\infty[)$;
    \item $f\in L^1(\mathbb{R})$, ni même $f\in C(\mathbb{R})\cap L^1(\mathbb{R})$, n'implique pas $f(x)\xrightarrow[x\to +\infty]{}0$;
    \item une suite $\suite{f_n}$ convergeant vers 0 dans $L^1([0;1])$ ne converge pas forcément vers 0 dans $L^\infty([0;1])$;
    \item une suite $\suite{f_n}$ peut vérifier $\forall n,\ \norm{f_n}_1=1$ et converger presque partout vers 0 sur $[0;+\infty[$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $f$ une fonction mesurable.
  \begin{enumerate}
    \item Soit $1\leq \alpha<\beta<\infty$. Montrer que pour tout $t\in[0,1]$ et $p=t\alpha+(1-t)\beta$, $|f|^p\leq |f|^\alpha+|f|^\beta$. En déduire que $L^\alpha(\mathbb{R})\cap L^\beta(\mathbb{R})\subset L^p(\mathbb{R})$.
    \item Montrer que $I_f=\{p\in[1,\infty[,\ f\in L^p(\mathbb{R})\}$ est un intervalle.
    \item Déterminer $I_f$ lorsque
    \begin{enumerate}
      \item $f(x)=\frac1x$ si $x\geq 1$, 0 sinon,
      \item $f(x)=\frac1{x(\ln x)^2}$ si $x\geq 2$, 0 sinon.
    \end{enumerate}
    En général, $I_f$ est-il ouvert ? Est-il fermé ?
    \item Soit $p\in\overline{I_f} \subset [1,\infty[$ et $\suite{p_n}$ une suite croissante qui converge vers $p$. Montrer que
    \[
      \lim_{n\to +\infty} \int|f(x)|^{p_n}dx= \int|f(x)|^{p}dx.
    \]
    Même question si $\suite{p_n}$ est décroissante. En déduire que $p\mapsto \norm{f}_p$ est continue sur $\overline{I_f}$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $(X,\mathcal{T},\mu)$ un espace mesuré. Montrer que si $\mu$ est une mesure finie alors
  \[
    L^{\infty}(\mu) \subset \bigcap_{p\geq 1} L^{p}(\mu),
  \]
  et, pour tout $f$,
  \[
    \lim_{p\rightarrow+\infty}\norm{f}_{p} = \norm{f}_{\infty}.
  \]
  \indication{$\mu\big(|f(x)|>\norm{f}_\infty - \varepsilon\big) > 0$, pour tout $\varepsilon>0$ .}
\end{exo}

% --------------------------------------------------
\begin{exo} \emph{(Rattrapage, juin 2021)}

  Soit $\Omega$ un sous ensemble borélien de $\mathbb{R}$.
  On note $\lambda$ la mesure de Lebesgue sur $\mathbb{R}$ et $\ind_{A}$ est la fonction indicatrice de $A\subset \mathbb{R}$.
  \begin{enumerate}
    \item Montrer que si $\lambda(\Omega)<\infty$, alors $L^{2}(\Omega)\subset L^{1}(\Omega)$.
  \end{enumerate}
  On suppose maintenant $L^{2}(\Omega)\subset L^{1}(\Omega)$ et on cherche à démontrer que $\lambda(\Omega)<\infty$.
  \begin{enumerate}[resume]
    \item Pour $n \in \mathbb{N}$ on définit $T_{n}:L^{2}(\Omega)\to L^{1}(\Omega)$ par $T_n(f)=f\ind_{[-n,n]}$. Montrer que $T_{n}$ est continue et pour tout $f \in L^{2}(\Omega)$, $\norm{T_n(f)}_1\leq \norm{f}_1 < \infty$.
    \item En déduire qu'il existe $C>0$ tel que pour tout $f \in L^{2}(\Omega)$, $\norm{f}_{1}\leq C\norm{f}_{2}$.
    \item En déduire que $\lambda(\Omega)<\infty$.
    \indication{Considérer la fonction $\ind_{\Omega\cap[-n,n]}$.}
  \end{enumerate}
\end{exo}


% ==================================================
\section{Propriétés des éléments de $L^p$}
% ==================================================

% --------------------------------------------------
\begin{exo}

  Soit $f\in L^1(\mathbb{R})$. Pour $n\in\mathbb N$, posons $g_n(t)\coloneqq \min\{|f(t)|, n\}$.
  \begin{enumerate}
    \item Montrer que pour tout $x\in X$, la suite $\suite{g_n(x)}$ est croissante et converge vers $|f(x)|$.
    \item Montrer que pour tout $\varepsilon>$, il existe $\delta>0$ tel que
    \[
      A\in\mathcal{B}(\mathbb{R}), \lambda(A)<\delta\Longrightarrow \int_A |f(x)|\,d\lambda(x)<\varepsilon.
    \]
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $f\in L^p(\mathbb{R})$, $1<p<+\infty$.
  \begin{enumerate}
     \item Montrer que l'on peut définir, pour tout $x\geq 0$, $F(x)=\int_0^xf(t)dt$ et justifier que $F(x)=_{+\infty}O(x^{1-\frac1p})$.
     \item Soit $\varepsilon>0$. Démontrer qu'il existe $a>0$ tel que $\left(\int_a^{+\infty} |f(t)|^pdt \right)^{\frac1p}<\varepsilon$.
     \item En déduire que $F(x)=_{+\infty}o(x^{1-\frac1p})$.
     \item Montrer que $\lim_{x\to 0}\int_0^x|f(t)|^pdt=0$.
     \item En déduire que $F(x)=_{0}o(x^{1-\frac1p})$.
  \end{enumerate}
\end{exo}


% ==================================================
\section{Inégalités}
% ==================================================

% --------------------------------------------------
\begin{exo}[.35]

  Soient $f$, $g\in L^3(\mathbb{R})$. Démontrer que $f^2g$ est intégrable.
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $\Omega$ une partie mesurable non vide  de $\mathbb{R}^N$ et $1\leq p,q,r\leq +\infty$ tels que $\frac1p+\frac1q=\frac1r$.
  \begin{enumerate}
    \item Montrer que pour tout $f\in L^p(\Omega)$ et tout $g\in L^q(\Omega)$, $fg$ appartient à $L^r(\Omega)$ et $\norm{fg}_r\leq \norm{f}_p\norm{g}_q$ \emph{(inégalité de Hölder généralisée)}.
    \item On suppose maintenant que $1\leq r<q\leq +\infty$ et que $\Omega$ est bornée. Montrer que pour tout $f\in L^q(\Omega)$, $f$ appartient à $L^r(\Omega)$ et $\norm{f}_r\leq |\Omega|^{\frac1r-\frac1q} \norm{f}_q$.\\
    \indication{Appliquer l'inégalité de Hölder généralisée avec un $p>0$ tel que $\frac1r=\frac1p+\frac1q$.}
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}[.7]

  Soit $(X,\mathcal{T},\mu)$ un espace mesuré, $f$, $g\in L^1(\mu)$. Montrer que $\norm{f}_1+\norm{g}_1=\norm{f+g}_1$ si et seulement si $f(x)\overline{g(x)}$ est un réel positif pour presque tout $x\in X$.
\end{exo}

% --------------------------------------------------
\begin{exo} \emph{(inégalité de Jensen)}

  Soit $(X,\mathcal A,\mu)$ un espace mesuré. On suppose que $\mu$ est une mesure de probabilité. Soient $a$ et $b$ des réels et $f\in L^1(\mu)$ une fonction à valeurs réelles telles que pour tout $x\in X$, on ait $a<f(x)<b$. Soit $\phi$ une fonction convexe sur $]a,b[$. On rappelle que $\phi$ vérifie l'inégalité des trois pentes :
  \[
    \forall x,y,z\text{ tels que } a< x<y<z<b,\text{ on a } \frac{\phi(y)-\phi(x)}{y-x}\leq \frac{\phi(z)-\phi(x)}{z-x} \leq \frac{\phi(z)-\phi(y)}{z-y}.
  \]
  \begin{enumerate}
    \item Soit $m\in]a,b[$. Montrer qu'il existe $\alpha\in\mathbb{R}$ tel que pour tout $t\in]a,b[$, $\phi(t)\geq \alpha (t-m) +\phi(m)$.
    \item Justifier que l'on peut choisir $m=\int_Xfd\mu$ dans la question précédente.
    \item Montrer que $\int_{\{\phi\circ f\leq 0\}} \phi\circ fd\mu>-\infty$. En déduire que soit $\phi\circ f$ appartient à $L^1(\mu)$, soit $\int_{\phi\circ f\geq 0}\phi\circ fd\mu=+\infty$. Ainsi, même si $\phi\circ f$ n'appartient pas à $L^1(\mu),$ on peut définir $\int_X\phi\circ f d\mu := \int_{\{\phi\circ f\leq 0\}} \phi\circ fd\mu+\int_{\{\phi\circ f\geq 0\}} \phi\circ fd\mu$ qui appartient à $\mathbb{R}\cup\{+\infty\}$.
    \item Établir l'inégalité de Jensen : $\int_X\phi\circ fd\mu \geq \phi\left(\int_Xfd\mu\right)$.
    \item \emph{(application)} Montrer que, pour tous nombres réels positifs $y_1,y_2,\dots,y_n$, on a
    \[
      \left(\prod_{i=1}^n y_i\right)^{1/n}\leq \frac{1}{n}\sum_{i=1}^n y_i.
    \]
  \end{enumerate}
\end{exo}


% ==================================================
\section{Convergence dans $L^p$}
% ==================================================

% --------------------------------------------------
\begin{exo}[.7]

  Soit $1\leq p<+\infty$ et $f\in L^p(\mathbb{R})$. Pour $t\in\mathbb{R}$, on définit $f_t:\mathbb{R}\to\mathbb{R}$ par $f_t(x)=f(x-t)$. Montrer que $t\mapsto \norm{f-f_t}_p$ est bornée et uniformément continue sur $\mathbb{R}$.\\
  \begin{indication}
    Utiliser la densité de $C_{c}(\mathbb{R})$ dans $L^p(\mathbb{R})$.
  \end{indication}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit $\Omega$ un ouvert non vide de $\mathbb{R}^N$, $f\in L^p(\Omega)$, $p<\infty$.
  \begin{enumerate}
  \item Pour $n\geq 1$ on définie la fonction suivante sur $\mathbb{R}$ :
  \[
    T_n(t)=
    \begin{dcases}
      t             & \text{ si } |t|\leq n, \\
      n\frac t{|t|} & \text{ sinon.}
    \end{dcases}
  \]
  Montrer que $\suite{T_n\circ f}$ converge vers $f$ dans $L^p$.
  \item Soit $\suite{\Omega_n}$ une suite croissante\footnote{$\Omega_n\subset \Omega_{n+1}$, $\forall n$} d'ouverts bornés tels que $\Omega=\cup_{n} \Omega_n$ et $\chi_n$ la fonction indicatrice de $\Omega_n$. Montrer que $\suite{\chi_n f}$ converge dans $L^p(\Omega)$ vers $f$.
  \item En déduire que $f$ est limite dans $L^p(\Omega)$ d'une suite de fonctions bornées à supports compacts dans $\Omega$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo}

  Soit  $(X,\mathcal{T},\mu)$ un espace mesuré, $f\in L^p(\mu)$ et $\suite{f_n}$ une suite dans $L^p(\mu)$ telles que $\lim_{k\to +\infty} \norm{f_k-f}_p=0$. Montrer que si $g:X\to \mathbb{C}$ est une fonction telle que $\lim_{k\to +\infty} f_k(x)=g(x)$ pour presque tout $x\in X$, alors $g(x)=f(x)$ pour presque tout $x\in X$.\\
  \indication{Montrqer qu'il existe une sous-suite $\suite[k]{f_{n_k}}$ qui converge presque partout vers $f$. Pour $p<\infty$ on peut considérer la suite qui vérifie $\norm{f-f_{n_k}}_{p} < \infty$.}

\end{exo}

% -----------------------------------------------
\begin{exo}

  Soit $\suiteN{f_{n}}$ la suite de fonctions définies par:
  \[
    f_{n}(x) = \frac{1}{\sqrt{n}} \mathbf{1}_{[n, 2n]}(x).
  \]
  \begin{enumerate}
    \item Montrer que $f_{n}$ converge faiblement vers $0$ dans $L^{2}([0, +\infty[)$, mais ne converge pas fortement dans $L^2([0, +\infty[)$.
    \item Montrer que $f_{n}$ converge fortement vers $0$ dans $L^{p}([0, +\infty[)$ pour $p>2$.
  \end{enumerate}
\end{exo}


\newpage
% ==================================================
\section{Opérateurs de $L^p$}
% ==================================================

% --------------------------------------------------
\begin{exo}

  Soit $p\in]1,+\infty[$ et soit $g\in L^{q}(\mathbb{R})$ où $q$ est l'exposant conjugué de $p$. Soit $T:L^p(\mathbb{R})\to\mathbb{C}$ défini par $T(f)=\int_\mathbb{R} f\overline g d\lambda$.
  \begin{enumerate}
    \item Montrer que $T$ est défini, continue et que $\norm{T}\leq \norm{g}_{q}.$
    \item En utilisant la fonction $f$ définie par $f(x)=g(x)|g(x)|^{q-2}$ si $g(x)\neq 0$, $f(x)=0$ sinon, montrer que $\norm{T}=\norm{g}_{q}$.
  \end{enumerate}
\end{exo}

% --------------------------------------------------
\begin{exo} \emph{(inégalité de Hardy)}

  Soit $1<p<\infty$. Le but de l'exercice est l'étude de l'application linéaire $T$ définie par
  \[
    Tf(x)=\frac{1}{x}\int_0^x f(t)\,dt, \qquad x>0, f\in L^p(]0,+\infty[).
  \]
  \begin{enumerate}
  \item Vérifier que $T$ est bien définie et positive, c.-à-d. pour tout $f$ et $g$ dans $L^p(]0,+\infty[)$, si $f\leq g$ alors $Tf\leq Tg$. En déduire que pour tout $f$ dans $L^p(]0,+\infty[$, $\abs{Tf}\leq T\abs{f}$.
  \item Montrer que pour tout $f$ dans $L^p(]0,+\infty[)$, $Tf$ est continu.
  \item Montrer que si $\suite{f_n}$ converge vers $f$ dans $L^p(]0,+\infty[)$ alors $\suite{Tf_n}$ converge simplement vers $Tf$.
  \item Supposons dans cette question que $f$ est continue, positive et à support bornée. Notons $F(x)=Tf(x)$.
  \begin{enumerate}
    \item Montrer que $F$ est de classe $C^1$ et satisfait $F(x)+x F'(x)=f(x)$.
    \item Montrer que  $\lim_{x\to 0}xF^p(x)=0$;
    \item Montrer que  $\lim_{x\to +\infty}xF^p(x)=0$;
    \item Montrer que
    \[
    \int_0^{+\infty} F^p(x)dx =\frac p{p-1}\int_0^{+\infty} F^{p-1}(x)f(x)dx.
    \]
    \item \label{q2} En déduire que $\norm{Tf}_p\leq \frac{p}{p-1}\norm{f}_p.$
  \end{enumerate}
  \item \label{q3} En utilisant le lemme de Fatou, déduire des questions précédentes l'inégalité de Hardy : pour toute fonction $f\in L^p(]0,+\infty[)$, on a
  \[
    \norm{Tf}_p\leq \frac{p}{p-1}\norm{f}_p
  \]
  \end{enumerate}
\end{exo}

% -----------------------------------------------
\begin{exo} \emph{(Examen, janvier 2022)}

Soient $p, q \in \mathbb{R}$ tels que $1 < p < \infty$ et $\frac{1}{p} + \frac{1}{q} = 1$.
Soit $E = C([0, 1], \mathbb{C})$ l'espace des fonctions continues sur $[0, 1]$ muni de la norme
\[
  \norm{f}_p = \left(\int_0^1 |f(x)|^p dx\right)^{\frac{1}{p}}.
\]
Soient $\alpha \leq \beta$ deux nombres dans $[0, 1]$. Soit $T : E \to \mathbb{C}$ l'application linéaire définie par
\[
  T(f) = \int_{\alpha}^{\beta} f(x) dx.
\]
\begin{enumerate}
\item\label{normT} Montrer que $T$ est continue et que $\normop{T} = (\beta  − \alpha)^{\frac{1}{q}}$.
\end{enumerate}

Soient $\suite{\alpha_n}$, $\suite{\beta_n}$, $\suite{\gamma_n}$ trois suites complexes avec $0 \leq \alpha_n \leq  \beta_n \leq  1$. Pour $n \in \mathbb{N}$, posons
\[
  T_n(f) = \gamma_n \int_{\alpha_n}^{\beta_n} f(x) dx\quad (f \in E).
\]
\begin{enumerate}[resume]
\item Montrer que pour tout $n \in \mathbb{N}$ l'application linéaire $T_n$ est continue et calculer la
norme de $T_n$. \indication{on pourra utiliser \eqref{normT}.}
\item Montrer que pour toute fonction $f \in E$, la suite $\suite{T_n(f)}$ est bornée si et seulement
si la suite $\suite{\gamma_n(\beta_n − \alpha_n)}$ est bornée.
\item Montrer que la suite $\suite{\normop{T_n}}$ est bornée si et seulement si la suite $\suite{\gamma_n(\beta_n −\alpha_n)^{\frac{1}{q}}}$ est bornée.
\item Montrer qu'on peut choisir $\suite{\alpha_n}$, $\suite{\beta_n}$, $\suite{\gamma_n}$ telles que
\[
  \forall f \in E, \sup_n \abs{T_n(f)} < \infty, \text{ mais } \sup_n \normop{T_n} = \infty.
\]
Expliquer pourquoi le théorème de Banach-Steinhaus ne pouvait pas être appliqué.
\end{enumerate}

\end{exo}


\end{document}
