\documentclass[a4paper,12pt,reqno]{amsart}
\usepackage{lille}
\input{m1analyse}

\lilleset{titre={Fiche 5 : Convolution, transformée de Fourier}}

\begin{document}


% ===============================================
\section{Calculs de convolutions}
% ===============================================

% ----------------------------------------------- 01 ← W05
\begin{exo}

  Calculer $f\ast g$ pour les fonctions suivantes :
  \begin{enumerate}
    \item $f=\mathds{1}_{[-1,1]}$ et $g=\mathds{1}_{[-a,a]}$, avec $a\geq 1$.
    \item $f(x)=\exp(\alpha x)\mathds{1}_{[0,+\infty[}(x)$ et $g(x)=\exp(\beta x)\mathds{1}_{[0,+\infty[}(x)$, avec $\alpha,\beta$ deux réels.
  \end{enumerate}
\end{exo}

% ----------------------------------------------- 02 ← W06
\begin{exo}

  Soit $f$ et $g$ deux fonctions continues sur $\mathbb{R}$ de classe $C^1$, bornées et dont les dérivées sont bornées.  On suppose que $f$ et $g'$ sont dans $L^1(\mathbb{R})$. Montrer que $f\ast g$ est bien définie et qu'elle est de classe $C^2$.

\end{exo}

% ----------------------------------------------- 03 ← W07
\begin{exo}

  Soit $f\in L^1(\mathbb{R})$ à support compact et $g:\mathbb{R}\to\mathbb{R}$ une fonction continue $\alpha$-höldérienne, $\alpha\in]0,1]$, c.-à-d. $\exists C>0,\ \forall x,y\in\mathbb{R},\ |g(x)-g(y)|\leq C|x-y|^\alpha$. Montrer que $f\ast g$ existe et est une fonction $\alpha$-höldérienne.
\end{exo}

% ----------------------------------------------- 04 ← W08
\begin{exo}
  \begin{enumerate}
    \item Soient $f$ et $g$ deux fonctions mesurables à support dans $[0,+\infty[$ et localement bornées (autrement dit, bornées sur tout compact de $[0,+\infty[$).
    Montrer que le produit de convolution $f\ast g$ est bien défini et localement bornée.
    \item Soit $f$ mesurable à support dans $[0,+\infty[$ et localement bornée. On pose $f^{\ast n}=\smash{\underbrace{f\ast\dots\ast f}_{n}}$. Montrer que pour tout $a>0$, tout $x\in[0,a]$ et tout $n\in\mathbb{N}^*$
    \[
      |f^{\ast n}(x)|\leq \Big(\sup_{[0,a]} |f|\Big)^{n} \frac{x^{n-1}}{(n-1)!}.
    \]
  \end{enumerate}
\end{exo}

% ----------------------------------------------- 05 ← W11
\begin{exo}

  Soit $\alpha>0$. Pour $x\in\mathbb{R}$, posons
  \[
    G_\alpha(x)=\frac{1}{\sqrt{2\pi\alpha}}\exp\left(-\frac{x^2}{2\alpha}\right).
  \]
  On rappelle que $\int_{-\infty}^{+\infty} e^{-x^2}dx=\sqrt\pi$.
  \begin{enumerate}
  \item Montrer que
  \[
    \int_{-\infty}^{+\infty}G_\alpha(x)\,dx=1,\quad \int_{-\infty}^{+\infty}xG_\alpha(x)\,dx=0,\quad \int_{-\infty}^{+\infty}x^2G_\alpha(x)\,dx=\alpha.
  \]
  \item Montrer que pour tout $\alpha,\beta>0$, on a $G_\alpha\ast G_\beta=G_{\alpha+\beta}$.
  \end{enumerate}
\end{exo}


% ===============================================
\section{Unité approchée et opérateurs de convolution}
% ===============================================


% ----------------------------------------------- 06 ← W10
\begin{exo}

  Pour $n\geq 1$, on définit $\phi_n:\mathbb{R}\longrightarrow \mathbb{C}$ par
  \[
    \phi_n(t)=\begin{cases}
      \frac{1}{\alpha_n}(1-t^2)^n & \text{si }|t|\leq 1 \\
      0                           & \text{si }|t|>1,
    \end{cases}
  \]
  où $\alpha_n=\int_{-1}^1(1-t^2)^n\,dt$.
  \begin{enumerate}
    \item Montrer que la suite $\suite{\phi_n}$ est une unité approchée\footnote{Appelé aussi \emph{approximation de l'identité} ou \emph{suite régularisante}.}.
    \item Soit $f\in C(\mathbb{R})$ vérifiant $f(x)=0$ si $x\notin I$, où $I=[-1/2,1/2]$. Montrer que $\phi_n\ast f$ est un polynôme sur $I$.
    \item En déduire le théorème de Weierstrass : toute fonction continue sur un intervalle compact de $\mathbb{R}$ est limite uniforme d'une suite de polynômes.
  \end{enumerate}
\end{exo}

% ----------------------------------------------- 07 ← W11bis
\begin{exo}

  Soient $f$ et $g$ deux fonctions mesurables sur $(\mathbb{R},\mathcal{B}(\mathbb{R}))$.
  \begin{enumerate}
    \item Soient $p\in[1,+\infty]$ et $q$ l'exposant conjugué de $p$. Montrer que si $f$ appartient à $L^p(\mathbb{R})$ et $g$ à $L^{q}(\mathbb{R})$ alors $f\ast g$ existe partout et vérifie $\norm{f\ast g}_\infty \leq\norm{f}_p\norm{g}_{q}$.
    \item On suppose que $f$ et $g$ sont dans $L^1(\mathbb{R})$. Montrer que $f\ast g$ est défini presque partout, appartient à $L^1(\mathbb{R})$ et vérifie $\norm{f\ast g}_1\leq \norm{f}_1\norm{g}_1$.
    \item Soient $f\in L^1(\mathbb{R})$ et $g\in L^p(\mathbb{R})$ avec $1<p<\infty$.
    \begin{enumerate}
      \item  En écrivant $|f(x-y)||g(y)|=|f(x-y)|^{\frac1p}|g(y)||f(x-y)|^{\frac1{q}}$, montrer que $f\ast g$ existe presque partout, est dans $L^p(\mathbb{R})$ et satisfait $\norm{f\ast g}_p\leq \norm{f}_1\norm{g}_p$.
      \item Soit $T_g:L^1(\mathbb{R})\to L^p(\mathbb{R})$ l'opérateur de convolution défini pour $h\in L^1(\mathbb{R})$ par $T_g(h)=h\ast g$. Montrer que $T_g$ est bien défini et continu. En testant $T_g$ contre une suite régularisante, montrer que $\norm{T_g}=\norm{g}_p$.
      \item Soit $S_f:L^p(\mathbb{R})\to L^p(\mathbb{R})$ défini pour $h\in L^p(\mathbb{R})$ par $S_f(h)=f\ast h$. Montrer que $S_f$ est bien défini et continu. Et si $f$ est positive montrer que $\norm{S_f}=\norm{f}_1$.\\
      \indication{Pour $f$ positive, montrer que la suite $\suite{f_n}$ définie par $f_n(x)=\frac1{\norm{f}_1}nf(nx)$ est une unité approchée. Puis considérer $S_{f}(h_n)$ pour $h_{n}(x)=h(\frac{x}{n})$.}

    \end{enumerate}
  \end{enumerate}
\end{exo}

% ----------------------------------------------- 08 ← W12
\begin{exo}

  Soit $f\in L^1(\mathbb{R})$.
  \begin{enumerate}
    \item Pour $h\in\mathbb{R}$, exprimer la fonction $x\mapsto \int_x^{x+h} f(t)dt$ comme le produit de convolution de $f$ par une fonction $\alpha_h$.
    \item On pose $F(x)=\int_0^x f(t)dt$, et on pose $G_h(x)=\frac{F(x+h)-F(x)}h$. Justifier que $G_h$ converge vers $f$ dans $L^1(\mathbb{R})$ quand $h$ tends vers 0.\\
    \indication{Montrer que $\suite[h>0]{\frac{\alpha_{h}}{h}}$ est une unité approchée.}
  \end{enumerate}
\end{exo}


% ===============================================
\section{Calculs de transformées de Fourier}
% ===============================================

% ----------------------------------------------- 09 ← W13
\begin{exo}

  Soit $\lambda>0$ et on pose
  \[
    f_\lambda(x)=e^{-\lambda\vert x\vert},\qquad \forall x\in\mathbb{R}.
  \]
  \begin{enumerate}
  \item Calculer la transformée de Fourier de $f_\lambda$.
  \item En déduire la transformée de Fourier de $x\mapsto \frac{1}{1+x^2}$.
  \item En déduire les valeurs des intégrales
  \[
    \int_{0}^\infty\frac{\cos(x)}{1+x^2}d\,x,\qquad \int_{0}^\infty\frac{\sin(x)x}{(1+x^2)^2}dx.
  \]
  \item Calculer $f_\lambda\ast f_\lambda$. Calculer ainsi la transformée de Fourier de $x\mapsto \frac{1}{(1+x^2)^2}$.
  \end{enumerate}
\end{exo}

% ----------------------------------------------- 10 ← W14
\begin{exo}

  Soit $\lambda>0$. On considère la fonction $f(t)=|t|e^{-\lambda|t|}$, $t\in\mathbb{R}$.
  \begin{enumerate}
    \item Justifier que $f\in L^1(\mathbb{R})$ et calculer la transformée de Fourier $F$ de $f$.
    \item Montrer que $F\in L^1(\mathbb{R})$.
    \item En déduire que l'on a
    \[
      \int_0^{+\infty}\frac{\lambda^2-u^2}{(\lambda^2+u^2)^2}\cos(u/2)\,du=\frac{\pi}{4}e^{-\lambda/2}.
    \]
  \end{enumerate}
\end{exo}

% ----------------------------------------------- 11 ← W21
\begin{exo}

  Soit
  \[
    f(x)=\begin{cases}
    1+x & \text{si }-1\leq x\leq 0 \\
    1-x & \text{si }0\leq x<1      \\
    0   & \text{sinon}.
    \end{cases}
  \]
  \begin{enumerate}
  \item Calculer la transformée de Fourier de $f$.
  \item Calculer $\int_0^{+\infty}\frac{\sin^4x}{x^4}\,dx$.\\
  \indication{on pourra remarquer que $f\in L^1(\mathbb{R})\cap L^2(\mathbb{R})$.}
  \end{enumerate}
\end{exo}


% ===============================================
\section{Applications de la transformée de Fourier}
% ===============================================

% ----------------------------------------------- 12 ← W16
\begin{exo}

  Soit $f\in L^1(\mathbb{R})$.
  \begin{enumerate}
    \item Montrer que pour tout $t$ :
    \[
      \int_\mathbb{R} f(x)e^{-itx^2}dx=\frac12\int_0^{+\infty}  \left(\frac{f(\sqrt u)}{\sqrt{u}}+\frac{f(-\sqrt u)}{\sqrt{u}} \right)e^{-iut}du.
    \]
    \item En déduire que $f\in L^1(\mathbb{R})$ est impaire\footnote{Une fonction $f\in L^1(\mathbb{R})$ est impaire, si $f(-x)=-f(x)$ pour presque tout $x$.} si et seulement si pour tout $t\in\mathbb{R}$,
    \[
      \int_\mathbb{R} f(x)e^{-itx^2} dx=0.
    \]
  \end{enumerate}
\end{exo}

% ----------------------------------------------- 13 ← W19
\begin{exo}[.7]

  Déterminer les couples $(f,c)\in\left( L^1(\mathbb{R})\cap C^1(\mathbb{R})\right)\times\mathbb{R}$ vérifiant, pour tout $x\in\mathbb{R}$, $f'(x)=f(x+c)$.
\end{exo}

% ----------------------------------------------- 14 ← W24
\begin{exo}\emph{(Formule sommatoire de Poisson)}

  Soit $f\in L^1(\mathbb{R})\cap C(\mathbb{R})$. On suppose que $f$ satisfait les deux conditions suivantes :
  \begin{enumerate}[(i)]
  \item il existe $M>0$ et $\alpha>1$ tels que
  \[
    |f(x)|\leq \frac{M}{(1+|x|)^\alpha},\qquad (x\in\mathbb{R});
  \]
  \item la série $\sum_{n\in\mathbb{Z}} |\widehat{f}(n)|$ converge.
  \end{enumerate}
  \begin{enumerate}
  \item Montrer que la série de fonctions $\sum_{n\in\mathbb{Z}} f(x+n)$ converge normalement sur tout compact de $\mathbb{R}$. On note $F$ la somme de cette série. En déduire que $F$ est continue et $1$-périodique.
  \item Montrer que $c_n(F):=\int_0^1 F(t)e^{-2i\pi nt}dt=\widehat{f}(n)$.
  \item En déduire que
  \[
    \sum_{n=-\infty}^{+\infty}\widehat{f}(n)=\sum_{n=-\infty}^{+\infty} f(n).
  \]
  \end{enumerate}
\end{exo}


% ===============================================
\section{Transformée de Fourier et convolution}
% ===============================================

% ----------------------------------------------- 15 ← W18
\begin{exo}\label{idempL1}
  \begin{enumerate}
    \item En utilisant la transformée de Fourier, montrer que l'algèbre $L^1(\mathbb{R})$ ne possède pas d'unité (i.e. il n'existe $g\in L^1(\mathbb{R})$ telle que $f\ast g=f$ pour tout $f\in L^1(\mathbb{R})$).
    \item Trouver toutes les fonctions $f\in L^1(\mathbb{R})$ vérifiant: $f*f=f$.
  \end{enumerate}
\end{exo}

% ----------------------------------------------- 16 ← W15
\begin{exo}

  Le but de cet exercice est de rechercher des fonctions $u\in L^1(\mathbb{R})$ telles que, pour presque tout $x\in\mathbb{R}$, on a
  \[
    u(x)=e^{-|x|}+\beta \int_{\mathbb{R}}e^{-|x-s|}u(s)\,ds,
  \]
  avec $\beta$ un réel strictement positif.
  \begin{enumerate}
    \item Écrire cette équation à l'aide d'un produit de convolution.
    \item En utilisant la transformée de Fourier, prouver qu'il existe une solution si et seulement si $\beta\in ]0,1/2[$; Montrer qu'alors cette solution est unique et la déterminer.
  \end{enumerate}
\end{exo}

% ----------------------------------------------- 17 ← W23
\begin{exo}\emph{(Espace de Wiener)}

  On note $W= L^1(\mathbb{R})\cap {\mathcal F} L^1(\mathbb{R})$ l'espace de Wiener constitué des fonctions intégrables qui sont également la transformée de Fourier d'une fonction intégrable.
  \begin{enumerate}
    \item Montrer que $f$ appartient à $W$ si et seulement si $f$ et $\widehat{f}$ appartiennent à $L^1(\mathbb{R})$\footnote{Avec la notation du cours $A(\mathbb{R})=\ensemble{f \in L^1(\mathbb{R})}{\widehat{f}\in L^1(\mathbb{R})}$, cette question démontre $W = A(\mathbb{R})$.}.
    \item Montrer que $W\subset C_0(\mathbb{R})$, l'espace des fonctions continues sur $\mathbb{R}$ qui tendent vers 0 à l'infini.
    \item Montrer que $L^1(\mathbb{R})\cap L^\infty(\mathbb{R})\subset \cap_{p\geq 1} L^p(\mathbb{R})$. En déduire que que $W\subset \cap_{p\geq 1} L^p(\mathbb{R})$. L'inclusion réciproque est-elle vraie ?
    \item Montrer que si $f$ appartient à $W$ alors $\widehat{f}$ appartient à $W$.
    \item Montrer que si $(f,g)\in W^2$ alors $f\ast g\in W$ et $fg\in W$.
    \item Pour $f\in W$, on pose $N(f)= \norm{f}_{L^1}+ \norm{\smash{\widehat{f}}\,}_{L^1}$. Montrer que $N$ est une norme sur $W$.
    \item Montrer que $W$ muni de la norme $N$ est un espace de Banach.
    \item Montrer que $\mathcal{D}(\mathbb{R})$\footnote{$\mathcal{D}(\mathbb{R})\coloneqq C^{\infty}(\mathbb{R})\cap C_{c}(\mathbb{R})$.} est inclus dans $W$. En déduire que $W$ est dense dans $L^p$ pour $p\in [1,+\infty[$.
    \item Montrer que $W$ est dense dans $C_0(\mathbb{R})$ muni de la norme uniforme.
  \end{enumerate}
\end{exo}


\end{document}
